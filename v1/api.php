<?php

// configure APP_PATH
require_once(getcwd().'/../int-fe.basepath.php');

// Linux is DEV / TEST and PROD server environment
if(strtoupper(php_uname("s")) === 'LINUX') {

    define("APP_PATH", $base_app_dir['LINUX']);
    
} // WIN is front-end developer machines 
else {

    define("APP_PATH", $base_app_dir['WIN']);    
}

// add classes
require_once(APP_PATH . "/php/core/Int_Router.php");
require_once(APP_PATH . "/php/core/Int_Log.php");

// create logger
$logger = new Int_Log("API");

try {
    
    // create the router
    $router = new Int_Router();
    
    $logger->log_debug("Process Request: ", $_REQUEST['request']);
    
    // process request
    return $router->do_process($_REQUEST['request']);
    
} catch (Exception $e) {
    
    $logger->log_error($e->getMessage());
}
?>