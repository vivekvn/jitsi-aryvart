/* global app, FB */

/**
 * INTELLIGENCE LTD ("COMPANY") CONFIDENTIAL Unpublished Copyright (c) 2016 INTELLIGENCE, All Rights
 * Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of COMPANY. The
 * intellectual and technical concepts contained herein are proprietary to COMPANY and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from COMPANY. Access to the source code
 * contained herein is hereby forbidden to anyone except current COMPANY employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering
 * such access.
 * 
 * The copyright notice above does not evidence any actual or intended publication or disclosure of
 * this source code, which includes information that is confidential and/or proprietary, and is a
 * trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR
 * PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF
 * COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.
 * THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY
 * ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */

app.run(['$rootScope', '$window', '$state', 'amMoment', 'DataService', 'HTTPService', 'DialogService',
    function (rootScope, window, state, amMoment, DataService, http, dialog) {

        // Set Moment.js locale
        amMoment.changeLocale(localStorage.locale);

        // Dialog State
        rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams, options) {

                // Cancel pending request when page changed
                DataService.cancel()
                http.cancel()

                switch (toState.name) {
                    case 'dialog':
                        dialog.show(toParams);
                        event.preventDefault();
                        break;
                    case 'load':
                        dialog.load(toParams.tasks).then(
                            function (res) {
                                //                                        console.log('loaded', res)
                            }, function (res) {
                                //                                console.log('load fail', res)
                                if (res.status == 401 || res.status == 403) {
                                    delete localStorage.token
                                    delete localStorage.code
                                    delete localStorage.signedIn
                                    location.href = '/login';
                                }
                            });
                        event.preventDefault();
                        break;
                    default:
                        $('.state-transition, .page-footer').css('opacity', 0)
                        break;
                }
            });
        rootScope.$on('$stateChangeSuccess', function (evt) {
            $('.state-transition, .page-footer').css('opacity', 1)
        });

    }]);
