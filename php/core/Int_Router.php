<?php

/**
 * INTELLIGENCE LTD ("COMPANY") CONFIDENTIAL Unpublished Copyright (c) 2016 INTELLIGENCE, All Rights
 * Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of COMPANY. The
 * intellectual and technical concepts contained herein are proprietary to COMPANY and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from COMPANY. Access to the source code
 * contained herein is hereby forbidden to anyone except current COMPANY employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering
 * such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or disclosure of
 * this source code, which includes information that is confidential and/or proprietary, and is a
 * trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR
 * PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF
 * COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.
 * THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY
 * ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
defined('APP_PATH') || exit('No direct script access allowed');

require_once('Int_Log.php');
require_once('Int_Server_Client.php');
require_once('Int_Session.php');

require_once(APP_PATH . '/php/common/HTTP_Constants.php');
require_once(APP_PATH . '/php/common/Constants.php');
require_once(APP_PATH . '/php/util/Security.php');
require_once(APP_PATH . '/php/util/Serializer.php');

/**
 * Intelligence Application Router.
 * 
 * 
 * @author Giorgio Desideri <giorgio@intelligence.in.th>
 */
class Int_Router
{

    private $logger = NULL;

    /**
     * List of allowed HTTP methods
     *
     * @var array
     */
    protected $allowed_http_methods = ['GET', 'DELETE', 'POST', 'PUT'];

    /**
     * Request method
     * 
     * @var string
     */
    protected $method = NULL;

    /**
     * Request headers.
     * 
     * @var array 
     */
    protected $headers = array();

    /**
     * Request parameters.
     * 
     * @var array 
     */
    protected $request_parameters = array();

    /**
     * Query parameters.
     * 
     * @var array
     */
    protected $query_parameters = array();

    /**
     * Routing table.
     * 
     * @var array
     */
    protected $rt = array();

    /**
     * Response format.
     * 
     * @var string 
     */
    protected $format = 'application/json';

    /**
     * Controller directory
     * 
     * @var string
     */
    protected $controller_dir = NULL;

    /**
     * Back-end Server URL
     * @var string
     */
    protected $server_prefix_url = NULL;

    /**
     * Whitelist
     * 
     * @var array 
     */
    protected $public_urls = array();

    /**
     * 
     * @param string $request
     * 
     * @throws Exception
     */
    public function __construct()
    {
        // init configuration
        $config = Int_Config::get_instance();

        $this->logger = new Int_Log("ROUTER");
        $this->logger->log_info("Start at: " . date('Y-m-d\TH:i:sO'));

        // load routing table
        $this->rt = $config->get_routing_table();

        // controller directory
        $this->controller_dir = $config->get_config("controller_dir");

        // url whitelist
        $this->public_urls = $config->get_config("public_urls");

        // server_url_prefix
        $this->server_prefix_url = $this->server_url_prefix = $config->get_config("server_url_url");

        // Disable XML Entity (security vulnerability)
        libxml_disable_entity_loader(TRUE);
    }

    /**
     * Destructor
     */
    public function __destruct()
    {
        // Get the current timestamp
        $this->logger->log_info("END at: " . date('Y-m-d\TH:i:sO'));
    }

    /**
     * Get request headers
     */
    private function get_headers()
    {
        $this->headers = getallheaders();
    }

    /**
     * Get request parameters
     */
    private function get_params()
    {
        switch ($this->method) {
            case "POST":
                foreach ($_POST as $key => $value) {
                    $this->request_parameters[$key] = $value;
                }
                break;

            case "GET":
                foreach ($_GET as $key => $value) {
                    $this->request_parameters[$key] = $value;
                }
                break;

            case "PUT":
                parse_str(file_get_contents('php://input'), $this->request_parameters);
                break;

            case "DELETE":
                parse_str(file_get_contents('php://input'), $this->request_parameters);
                break;

            default:
                break;
        }
    }

    /**
     * Parse request
     * 
     * @param string $request URI
     * 
     * @return array 
     *      'method' => HTTP method,     
     *      'class' => controller class,
     *      'class_method' => method name of controller class
     */
    private function parse_request($request = '')
    {
        $matches = array();

        $this->logger->log_debug("Request to process: ", $request);

        // Loop through the route array looking for wildcards
        foreach ($this->rt as $key => $value) {

            // Convert wildcards to RegEx
            $key = str_replace(array(':any', ':num'), array('[^/]+', '[0-9]+'), $key);

            // matches
            $res = preg_match('#^' . $key . '$#', $request, $matches);

            // skip
            if (!$res || $res === 0)
            {
                continue;
            }
            // check if exist parameter to associate
            if (array_key_exists('param_names', $value))
            {
                // associate
                for ($index = 0; $index < count($value['param_names']); $index++) {
                    $this->query_parameters[$value['param_names'][$index]] = $matches[$index + 1];
                }
            }

            // check method
            if (!array_key_exists('method', $value))
            {
                $value['method'] = 'GET';
            }

            return array(
                'method' => $value['method'],
                'class' => $value['class'],
                'class_method' => $value['class_method']
            );
        }
    }

    /**
     * Process request
     * 
     * @param type $request
     * 
     * @return void
     */
    public function do_process($request = '')
    {
        // get request method
        $this->method = filter_input(INPUT_SERVER, 'REQUEST_METHOD');

        // check methods
        if (!in_array($this->method, $this->allowed_http_methods))
        {
            $this->logger->log_error("Method Not Allowed");
            header("HTTP/1.1 405 Method Not Allowed");
            return;
        }

        try {
            // check DDoS
            $this->check_ddos();

            // get headers
            $this->get_headers();

            // check authorization
            if (!$this->authorize($request))
            {
                $this->logger->log_error("Authorization failed");
//                header("HTTP/1.1 403 Forbidden");
                return;
            }

            // get parameters
            $this->get_params();

            // get query params
            $result = $this->parse_request($request);

            $this->logger->log_trace("Parse request result: ", $result);

            // check method
            if (strcasecmp($this->method, $result['method']) != 0)
            {
                $this->logger->log_error(
                        "Method Not Allowed. Expect " . $this->method . " instead of " . $result['method']);
                header("HTTP/1.1 405 Method Not Allowed");
                return;
            }

            // Request
            $ctrl_request = new stdClass();
            $ctrl_request->headers = $this->headers;
            $ctrl_request->url = $request;
            $ctrl_request->params = array_merge($this->request_parameters, $this->query_parameters);

            // Response
            $ctrl_response = new stdClass();

            $this->logger->log_debug("CLASS: ", $result['class']);

            // controller class
            $ctrl_class = APP_PATH . $this->controller_dir . $result['class'] . '.php';
            $this->logger->log_debug("Controller class to load: ", $ctrl_class);

            // check controller class
            if (!file_exists($ctrl_class))
            {
                $this->logger->log_warn("Controller class " . $ctrl_class . " NOT EXIST.");
                header("HTTP/1.1 404 Not Found");
                return;
            }

            // include
            require_once($ctrl_class);

            // explode
            $ctrl_els = explode('/', $result['class']);

            // get last paramerter
            $class = new ReflectionClass(end($ctrl_els));

            // call instance
            $instance = $class->newInstanceArgs(array($ctrl_request, $ctrl_response, $this->format));

            // check
            if (method_exists($instance, $result['class_method']))
            {
                $this->logger->log_info("Calling method: ", $result['class_method']);

                // get method
                $refl_method = $class->getMethod($result['class_method']);

                // invoke
                return $refl_method->invoke($instance);
            } else
            {
                $this->logger->log_info(
                        "Method " . $result['class_method'] . " not found inside class " . $result['class']);

                header("HTTP/1.1 404 Not Found");
                return;
            }
        } catch (Exception $e) {
            $this->logger->log_error($e->getMessage());

            header("HTTP/1.1 500 Server Internal Error");
            return;
        }
    }

    private function check_ddos()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else
        {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $this->logger->log_trace("Client IP: " . $ip);
    }

    /**
     * Check authorization against the headers and request
     * 
     * @param type $request
     * 
     * @return boolean
     */
    private function authorize($request = '')
    {
        // empty request : NOT ALLOWED
        if (empty($request))
        {
            $this->logger->log_warn("Authorize. Request is empty.");
            return FALSE;
        }

        if ((!array_key_exists(Constants::INT_FE_HEADER_NAME, $this->headers) || !isset($_COOKIE[session_name()])) && $request == 'auth/session')
        {
            return TRUE;
        }

        // No Token
        if (!isset($_COOKIE[session_name()]))
        {
            header("HTTP/1.1 403 Forbidden");
            return FALSE;
        }

        $session = Int_Session::getInstance($_COOKIE[session_name()]);
//        $session = new Int_Session($this->headers[Constants::INT_FE_HEADER_NAME]);
        $session->start();

        if (!empty($_SESSION['deleted_time']) && $_SESSION['deleted_time'] < time() - 180)
        {
            header("HTTP/1.1 401 Unauthorized");
            return FALSE;
        }


        $user_credential = !empty($session->get('USER_DATA'));

        $is_public_url = $this->is_public_url($request);

        if (!$is_public_url && !$user_credential)
        {

            // No credential
            if (array_key_exists(Constants::INT_FE_HEADER_NAME, $this->headers))
            {
                header("HTTP/1.1 401 Unauthorized");
//                header("HTTP/1.1 403 Forbidden");
            } else
            {
                header("HTTP/1.1 403 Forbidden");
            }

            return FALSE;
        }
        if (!empty($_SESSION['expiration']))
        {
            if ($_SESSION['expiration'] < time())
            {
                $_SESSION['deleted_time'] = time();
                session_destroy();
                header("HTTP/1.1 401 Unauthorized");
                return FALSE;
            }
            $session->set('expiration', time() + Constants::SESSION_EXPIRATION_TIME);
        }

        $this->logger->log_info("Authorized. Go ahead.");

        return TRUE;
    }

    /**
     * Get User credential.
     * 
     * @return array or NULL
     */
    private function get_user_credential()
    {
        // No Token
        if (!array_key_exists(Constants::INT_FE_HEADER_NAME, $this->headers))
        {
            return NULL;
        }

        // init the sessin driver
        $session = new Int_Session($this->headers[Constants::INT_FE_HEADER_NAME]);
        session_start();

        // get user data
        $user_data = $session->get(Constants::USER_DATA_SESSION_KEY);

        // json decode
        if (!empty($user_data))
        {
            return TRUE;
        } else
        {
            return FALSE;
        }
    }

    /**
     * Is public URL.
     * 
     * @param string $request
     * 
     * @return TRUE if request is public, otherwise FALSE.
     */
    private function is_public_url($request)
    {

        // Check public urls
        foreach ($this->public_urls as $url) {

            // check as regular expression
            preg_match("#" . $url . "#", $request, $matches);


            if (!empty($matches))
            {
                return TRUE;
            }
        }

        return FALSE;
    }

}
