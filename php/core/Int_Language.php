<?php

/**
 * INTELLIGENCE LTD ("COMPANY") CONFIDENTIAL Unpublished Copyright (c) 2016 INTELLIGENCE, All Rights
 * Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of COMPANY. The
 * intellectual and technical concepts contained herein are proprietary to COMPANY and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from COMPANY. Access to the source code
 * contained herein is hereby forbidden to anyone except current COMPANY employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering
 * such access.
 * 
 * The copyright notice above does not evidence any actual or intended publication or disclosure of
 * this source code, which includes information that is confidential and/or proprietary, and is a
 * trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR
 * PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF
 * COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.
 * THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY
 * ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
defined('APP_PATH') || exit('No direct script access allowed');

require_once('Int_Log.php');

/**
 * Description of Int_Language
 *
 * @author Giorgio Desideri - giorgio@intelligence.in.th
 */
class Int_Language
{

    private static $instance = NULL;
    
    /**
     * Logger.
     * 
     * @var Int_Log 
     */
    private $logger = NULL;

    /**
     * Dictionary.
     * 
     * @var array 
     */
    private $dictionary = array();
    
    /**
     * 
     */
    private function __construct()
    {
        $this->logger = new Int_Log(strtoupper("Int_Language"));

        // 
        // ATTENTION: this piece of code is realized to keep the back-compatibility.
        // load english folder
        $this->load_lang_file('english');

        // load thai folder
        $this->load_lang_file('thai');
    }

    private function __clone()
    {
        // keep here.
    }

    private function __wakeup()
    {
        throw new Exception("Cannot unserialize singleton");
    }

    /**
     * Load language file.
     * 
     * @param string $language name of directory to load the file. It is the same of language name.
     */
    private function load_lang_file($language = 'english')
    {
        $lang_file = '/php/languages/' . strtolower($language) . '/messages_lang.php';

        $this->logger->log_info("Load Language file: " . $lang_file);

        require_once(APP_PATH . $lang_file);
        
        // add to dictionary
        $this->dictionary[strtolower($language)] = $lang;
    }

    /**
     * Get instance
     * 
     * @return Int_Language
     */
    public static function get_instance()
    {
        // check
        if (static::$instance == NULL)
        {
            static::$instance = new Int_Language();
        }

        return static::$instance;
    }

    /**
     * Get message.
     * 
     * @param type $language
     * @param type $message_key
     * 
     * @return message
     */
    public function get_message($language, $message_key) {
        
        if(!array_key_exists($language, $this->dictionary)) {
            return '';
        }
        
        if(!array_key_exists($message_key, $this->dictionary[strtolower($language)])) {
            return '';
        }
        
        return $this->dictionary[strtolower($language)][$message_key];
    }
}
