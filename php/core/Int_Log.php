<?php

/**
 * INTELLIGENCE LTD ("COMPANY") CONFIDENTIAL Unpublished Copyright (c) 2016 INTELLIGENCE, All Rights
 * Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of COMPANY. The
 * intellectual and technical concepts contained herein are proprietary to COMPANY and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from COMPANY. Access to the source code
 * contained herein is hereby forbidden to anyone except current COMPANY employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering
 * such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or disclosure of
 * this source code, which includes information that is confidential and/or proprietary, and is a
 * trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR
 * PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF
 * COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.
 * THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY
 * ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
defined('APP_PATH') || exit('No direct script access allowed');

require_once('Int_Config.php');

require_once(APP_PATH . '/php/util/StringUtil.php');

/**
 * Giorgio Desideri <giorgio@intelligence.in.th>
 *
 * String helper.
 */
class Int_Log
{

    /**
     * Log file.
     * 
     * @var string
     */
    private $log_file = NULL;

    /**
     * Log level.
     * 
     * @var int 
     */
    private $log_level = NULL;

    /**
     * Log levels codes.
     * 
     * @var array
     */
    private $log_levels = array(
      'NONE' => 0,
      'ERROR' => 1,
      'WARN' => 2,
      'INFO' => 3,
      'DEBUG' => 4,
      'TRACE' => 5,
    );

    /**
     *
     * @var string
     */
    private $caller = NULL;

    /**
     * COnstructor.
     * 
     * @param string $caller
     */
    function __construct($caller = '')
    {
        // get config
        $config = Int_Config::get_instance();

        // set file
        $this->set_log_file($config->get_config('log_file_path'),
          $config->get_config('log_file_name'));

        // update PHP config
        ini_set("error_log", $this->log_file);

        // log level
        $level = $config->get_config('log_level');

        if (array_key_exists($level, $this->log_levels))
        {
            $this->log_level = $this->log_levels[$level];
        } else
        {
            // Default is INFO
            $this->log_level = 3;
        }

        // caller
        $this->caller = $caller;
    }

    /**
     * Set log file and level.
     * 
     * @param string $file_path
     * @param string $file_name
     * 
     * @throws Exception
     */
    private function set_log_file($file_path, $file_name)
    {
        // check file path
        if (!file_exists($file_path))
        {
            // create directory
            $bool = mkdir($file_path, 0755, true);

            if (!$bool)
            {
                throw new Exception("Cannot create log file directory. log_file_path : " . $file_path);
            }
        }

        // check directory name
        if (!StringUtil::ends_with(DIRECTORY_SEPARATOR, $file_path))
        {
            $file_path = $file_path . DIRECTORY_SEPARATOR;
        }

        // check
        if ($file_name == NULL || trim($file_name) == '')
        {
            // set default name
            $file_name = "default_int_fe_file_name.log";
        }

        // set file
        $this->log_file = $file_path . $file_name . "_" . date("Y-m-d") . ".log";

        // check
        if (!file_exists($this->log_file))
        {
            $file = fopen($this->log_file, 'w');
            fclose($file);
        }
    }

    /**
     * Log ERROR message.
     *
     * @param string $message
     * @param unknown $variable
     */
    public function log_error($message = "", $variable = NULL)
    {
        $this->log_msg("ERROR", $message, $variable);
    }

    /**
     * Log WARN message.
     *
     * @param string $message
     * @param unknown $variable
     */
    public function log_warn($message = "", $variable = NULL)
    {
        $this->log_msg("WARN", $message, $variable);
    }

    /**
     * Log INFO message.
     *
     * @param string $message
     * @param unknown $variable
     */
    public function log_info($message = "", $variable = NULL)
    {
        $this->log_msg("INFO", $message, $variable);
    }

    /**
     * Log DEBUG message.
     *
     * @param string $message
     * @param unknown $variable
     */
    public function log_debug($message = "", $variable = NULL)
    {
        $this->log_msg("DEBUG", $message, $variable);
    }

    /**
     * Log TRACE message.
     * 
     * @param type $message
     * @param type $variable
     */
    public function log_trace($message = "", $variable = NULL)
    {
        $this->log_msg("TRACE", $message, $variable);
    }

    /**
     * Log message
     *
     * @param string $level
     *            log level ( ERROR, WARN, INFO, DEBUG, TRACE )
     *
     * @param string $message
     *            log message
     *
     * @param mixed $variable
     *            variable to append printed with var_dump function
     */
    protected function log_msg($level = "", $message = "", $variable = NULL)
    {

        // init variable
        $log_message_str = PHP_EOL . "[" . date('Y-m-d\TH:i:sO') . "] [$level] [" . $this->caller . "] - ";

        // append message
        $log_message_str .= $message;

        // check variable
        if ($variable != NULL)
        {
            // open buffer
            ob_start();

            // var dump
            var_dump($variable);

            // append to log message string
            $log_message_str .= " - " . ob_get_clean();
        }

        // check
        if ($this->log_level >= $this->log_levels[$level])
        {
            file_put_contents($this->log_file, $log_message_str, FILE_APPEND | LOCK_EX);
        }
    }

}
