<?php

/**
 * INTELLIGENCE LTD ("COMPANY") CONFIDENTIAL Unpublished Copyright (c) 2016 INTELLIGENCE, All Rights
 * Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of COMPANY. The
 * intellectual and technical concepts contained herein are proprietary to COMPANY and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from COMPANY. Access to the source code
 * contained herein is hereby forbidden to anyone except current COMPANY employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering
 * such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or disclosure of
 * this source code, which includes information that is confidential and/or proprietary, and is a
 * trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR
 * PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF
 * COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.
 * THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY
 * ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
defined('APP_PATH') || exit('No direct script access allowed');

require_once('Int_Log.php');
require_once('Int_Config.php');

require_once(APP_PATH . '/php/common/Constants.php');

/**
 * The Intelligence MEMCACHED library.
 * 
 * @author Giorgio Desideri <giorgio@intelligence.in.th>
 *         
 */
class Int_Memcached {

    const WINDOWS_CLIENT = 'WINDOW$';
    const LINUX_CLIENT = 'LINUX';

    private $client = NULL;
    private $client_type = NULL;
    private $expire_time = 0;
    private $compression = FALSE;
    private $logger = NULL;
    private $servers = NULL;

    /**
     * Constructor
     */
    public function __construct() {
        $this->logger = new Int_Log(strtoupper("Int_Memcached"));

        // configuration
        $config = Int_Config::get_instance();

        // client type
        $this->client_type = $config->get_config('memcached_type');

        $this->logger->log_trace("Memcached client type: ", $this->client_type);

        switch ($this->client_type) {
            case Int_Memcached::WINDOWS_CLIENT:
                $this->client = new Memcache();
                break;

            default:
            case Int_Memcached::LINUX_CLIENT:
                $this->client = new Memcached();
                break;
        }

        // default expire time
        $this->expire_time = $config->get_config('memcached_exire_time');

        // compression
        $this->compression = $config->get_config('memcached_compression');

        // connect
        $this->servers = $config->get_config('memcached_servers');
    }

    /**
     * Destruct
     */
    public function __destruct() {
        $this->close();
    }

    /**
     * Connect
     */
    private function connect() {
        foreach ($this->servers as $key => $server) {

            extract($server);

            return $this->client->addServer($host, $port, $weight);
        }
    }

    /**
     * Get stats.
     *
     * @return mixed
     */
    public function get_stats() {
        $this->connect();

        switch ($this->client_type) {
            case Int_Memcached::WINDOWS_CLIENT:
                $stats = $this->client->getStats();
                break;

            default:
            case Int_Memcached::LINUX_CLIENT:
                $stats = $this->client->getStats();
                break;
        }

        $this->close();

        return $stats;
    }

    /**
     * Get the object inside memcached.
     *
     * @param string|NULL $key
     *            the object key
     * @param boolean $decode_json
     *            default is TRUE.
     *
     * @return mixed the object, otherwise FALSE.
     */
    public function get($key, $decode_json = TRUE) {
        $data = $this->get_extended($key);

        // check result
        if (is_string($data) && $decode_json) {
            return json_decode($data, TRUE);
        } else {
            return $data;
        }
    }

    /**
     * Get the object from memcached.
     * 
     * @param string $key
     * 
     * @return array|string|object
     */
    public function get_extended($key, $force_decode = TRUE) {
        $this->connect();

        $this->logger->log_info('Int_Memcached::get_extended(key [' . $key . ']');

        if ($key == NULL) {
            $this->logger->log_error('MEMCACHED. KEY is NULL');
            $this->close();
            return NULL;
        }

        // check if key is a string
        if (!is_string($key)) {
            $this->logger->log_error('MEMCACHED. KEY is NOT a STRING');
            $this->close();
            return NULL;
        }

        // return the object
        $data = $this->client->get($key);

        $this->logger->log_trace('MEMCACHED. Compressed Data ', $data);

        // check data
        if ($data === FALSE) {
            // try to retrieve the other parts
            $index = 0;
            $items = array();
            $exit_flag = TRUE;

            do {
                // chunk key
                $chunk_key = $key . "_" . $index;

                // chunk value
                $chunk_value = $this->client->get($chunk_key);

                // check
                if ($chunk_value === FALSE) {
                    // go away from here
                    $exit_flag = FALSE;
                }

                // add to array
                $items[$chunk_key] = $chunk_value;

                // increase index
                $index++;
            } while ($exit_flag);

            if (empty($items)) {
                $this->close();
                return NULL;
            }

            $compressed_value = "";

            // merge all values
            foreach ($items as $t_key => $t_value) {
                $compressed_value .= $t_value;
            }

            if ($compressed_value === '' || $compressed_value == NULL) {
                $this->close();
                return NULL;
            }

            // decompress
            if ($force_decode) {
                // decompress
                $value = base64_decode($compressed_value);
            } else {
                $value = $compressed_value;
            }

            $this->logger->log_trace('MEMCACHED. Decompress Data ', $value);
            $this->close();
            return $value;
        } // single key with data 
        else {
            if ($data === '' || $data == NULL) {
                return NULL;
            }

            // decompress
            if ($force_decode) {
                // decompress
                $value = base64_decode($data);
            } else {
                $value = $data;
            }

            $this->logger->log_trace('MEMCACHED. Decompress Data ', $value);
            $this->close();
            return $value;
        }
    }

    /**
     * Insert a new object inside memcached.
     *
     * @param string $key
     *            the object key
     * 
     * @param mixed $value
     *            the object
     * 
     * @param int|NULL $expiration
     *            expiration time in seconds
     *
     * @return boolean TRUE|FALSE
     */
    public function set($key, $value, $encode_json = TRUE, $expiration = NULL) {
        if ($encode_json === TRUE) {
            $data = json_encode($value);
        } else {
            $data = $value;
        }

        return $this->set_extended($key, $data, $expiration);
    }

    /**
     * Compress and store the value inside memcached. If value is bigger than max row lenght, value
     * will be splitted.
     * 
     * @param string $key
     * @param mixed $value
     * @param int $expiration
     * @param bool $force_encode
     * 
     * @return array that contains the keys of values in memcached, otherwise NULL. 
     */
    public function set_extended($key, $value, $expiration = NULL, $force_encode = TRUE) {
        $this->connect();

        $this->logger->log_info('Int_Memcached::set_extended(key [' . $key . ']');

        // check
        if ($value == NULL) {
            $this->logger->log_error('MEMCACHED. Value to store is NULL');
            $this->close();
            return NULL;
        }

        // expiration time
        if ($expiration == NULL) {
            $expiration = time() + $this->expire_time;
        }

        $this->logger->log_trace('MEMCACHED. Expiration is : ', $expiration);

        // convert as string
        if (is_array($value) || is_object($value)) {
            // encode as json
            $data_str = json_encode($value);

            $this->logger->log_debug("MEMCACHED. Array or Object detected, encode as JSON", $data_str);
        } else {
            // copy
            $data_str = $value;

            $this->logger->log_debug("MEMCACHED. Leave as input", $data_str);
        }

        // compress
        if ($force_encode) {
            // debug
            $this->logger->log_debug("MEMCACHED. Encode Base64.");

            // compress
            $data_compress = base64_encode($data_str);
        } else {
            // not compress
            $data_compress = $data_str;
        }

        // check key
        if (strlen($key) > 240) {
            $this->logger->log_debug("MEMCACHED. Key too long, more than 240 chars, will replaced");

            // create hash of 128 chars (SHA-512) as key
            $key = hash('sha512', $key);

            $this->logger->log_debug("MEMCACHED. Key replaced with : [" . $key . "]");
        }

        // check size
        if (strlen($data_compress) > Constants::MEMCACHED_CHUNK_SIZE) {
            $this->logger->log_debug("MEMCACHED. Data split. "
                    . "Bigger than " . Constants::MEMCACHED_CHUNK_SIZE . " bytes");

            // split
            $pieces = str_split($data_compress, Constants::MEMCACHED_CHUNK_SIZE);

            // create array
            $items = array();

            // iterate on chunks
            for ($index = 0; $index < count($pieces); $index++) {

                // chunk key
                $chunk_key = $key . "_" . $index;

                // add to array
                $items[$chunk_key] = $pieces[$index];
            }
        } else {
            $this->logger->log_debug("MEMCACHED. Data OK - No split required. "
                    . "Less than " . Constants::MEMCACHED_CHUNK_SIZE . " bytes");

            // create array
            $items = array($key => $data_compress);
        }

        $this->logger->log_trace("MEMCACHED. Data", $items);

        // set
        switch ($this->client_type) {
            case Int_Memcached::WINDOWS_CLIENT:

                foreach ($items as $t_key => $t_value) {
                    // save
                    $result = $this->client->set($t_key, $t_value, 0, $expiration);

                    // check
                    if ($result === FALSE) {
                        // break and return FALSE
                        break;
                    }
                }
                break;

            default:
            case Int_Memcached::LINUX_CLIENT:
                $result = $this->client->setMulti($items, $expiration);
                break;
        }

        $this->logger->log_debug("MEMCACHED. Result of set: ", $result);
        $this->close();

        if ($result === FALSE) {
            return NULL;
        } else {
            return array_keys($items);
        }
    }

    /**
     * Remove object from memcached.
     *
     * @param string $key
     *
     * @return boolean
     */
    public function remove($key) {
        $this->connect();

        // check key
        if ($key == NULL) {
            $this->logger->log_error('MEMCACHED. KEY is NULL');
            $this->close();

            return FALSE;
        }

        // check if key is a string
        if (!is_string($key)) {
            $this->logger->log_error('MEMCACHED. KEY is NOT a STRING');
            $this->close();

            return FALSE;
        }

        // delete
        $result = $this->client->delete($key);

        $this->close();

        return $result;
    }

    /**
     * Close the client
     */
    public function close() {
        // set
        switch ($this->client_type) {
            case Int_Memcached::WINDOWS_CLIENT:

                // close        
                $this->client->close();
                break;

            default:
            case Int_Memcached::LINUX_CLIENT:
                // close        
                $this->client->quit();
                break;
        }
    }

    /**
     * Extends the expiration time of other 30 minutes.
     * 
     * @param type $key
     * @param type $new_expiration_time default is 30 minutes
     */
    public function extend($key, $new_expiration_time = 1800) {
        $this->connect();

        // get value
        $value = $this->get($key, FALSE);

        // check
        if ($value !== NULL) {
            $this->set($key, $value, $new_expiration_time);
        }

        $this->close();
    }

}
