<?php

/**
 * INTELLIGENCE LTD ("COMPANY") CONFIDENTIAL Unpublished Copyright (c) 2016 INTELLIGENCE, All Rights
 * Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of COMPANY. The
 * intellectual and technical concepts contained herein are proprietary to COMPANY and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from COMPANY. Access to the source code
 * contained herein is hereby forbidden to anyone except current COMPANY employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering
 * such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or disclosure of
 * this source code, which includes information that is confidential and/or proprietary, and is a
 * trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR
 * PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF
 * COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.
 * THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY
 * ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
defined('APP_PATH') || exit('No direct script access allowed');

require_once(APP_PATH . '/php/common/HTTP_Constants.php');
require_once(APP_PATH . '/php/common/Constants.php');

require_once('Int_Log.php');
require_once('Int_Config.php');

/**
 * The Intelligence Rest Client.
 * 
 * @author Giorgio Desideri <giorgio@intelligence.in.th>
 */
class Int_Server_Client
{

    private $logger = NULL;

    /**
     */
    public function __construct()
    {
        $this->logger = new Int_Log(strtoupper("Int_Server_Client"));
    }

    /**
     *
     * @param type $url
     * @return type
     */
    private function is_valid_url($url)
    {
        // check input
        if ($url == null || $url === '')
        {

            $this->logger->log_error("Url is NOT valid [" . $url . "]");

            return false;
        } else
        {
            $this->logger->log_info("Url is valid [" . $url . "]");

            return true;
        }
    }

    /**
     *
     * @param type $httpcode
     * @param type $response
     *
     * @return type ARRAY. Described as following
     *         array ( 'httpcode' => HTTP Response code,
     *         'response' => HTTP Response body
     *         );
     */
    private function compute_response($httpcode, $response = null)
    {
        $result = array();

        // check http code
        if ($httpcode != null)
        {
            $this->logger->log_debug("HttpCode is: [" . $httpcode . "]");

            $result[Constants::HTTP_CODE_KEY_NAME] = $httpcode;
        } else
        {
            $this->logger->log_error("HttpCode is NULL or empty");

            $result[Constants::HTTP_CODE_KEY_NAME] = '';
        }

        // check response
        if ($response != null)
        {

            $result[Constants::HTTP_RESULT_KEY_NAME] = $response;
        } else
        {
            $this->logger->log_info("Response is NULL or empty");

            $result[Constants::HTTP_RESULT_KEY_NAME] = null;
        }

        return $result;
    }

    /**
     *
     * @param string $auth
     * @return string
     */
    private function check_auth($auth)
    {
        if ($auth == null || empty($auth))
        {
            $auth = array(
                'username' => "",
                'token' => ""
            );

            $this->logger->log_debug("Authen is null");
        }

        $this->logger->log_debug("Username : [" . $auth['username'] . "] ");
        $this->logger->log_debug("Token : [" . $auth['token'] . "] ");

        return $auth;
    }

    /**
     * Execute GET call.
     *
     * @param type $url
     *            URL to call
     *
     * @return type ARRAY. Described as following
     *         array ( 'httpcode' => HTTP Response code,
     *         'response' => HTTP Response body
     *         );
     */
    public function execute_get($url, $credentials = array())
    {
        // check
        if (!$this->is_valid_url($url))
        {
            return;
        }

        // check authentication credentials
        $auth = $this->check_auth($credentials);

        $ch = curl_init();

        // Set some options - we are passing in a useragent too here
        curl_setopt_array($ch,
                array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => "Front-end Curl Agent",
            CURLOPT_HTTPHEADER => array(
                'authorized_user:' . $auth['username'],
                'authorized_token:' . $auth['token']
            )
        ));

        // Send the request & save response to $resp
        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Close request to clear up some resources
        curl_close($ch);

        return $this->compute_response($httpcode, $response);
    }

    /**
     *
     * @param string $url
     *            URL to call
     * @param array $data
     * @param array $credentials
     * @param boolean $convert
     *            to json format, default is TRUE
     *
     * @return array. Described as following
     *         array ( 'httpcode' => HTTP Response code,
     *         'response' => HTTP Response body
     *         );
     */
    public function execute_post($url, $data = null, $credentials = array(), $convert = TRUE)
    {

        // check input
        if (!$this->is_valid_url($url))
        {
            return;
        }

        // check authentication credentials
        $auth = $this->check_auth($credentials);

        $json_data = null;

        // check data
        if ($data != null)
        {

            if ($convert)
            {
                // encode data in json format
                $json_data = json_encode($data);
            } else
            {
                foreach ($data as $key => $value) {
                    $json_data .= $key . "=" . $value . "&";
                }
            }
        }

        if (strlen($json_data) > 1000)
        {
            $this->logger->log_debug("JSON data [" . substr($json_data, 0, 1000) . "]");
        } else
        {
            $this->logger->log_debug("JSON data [" . $json_data . "]");
        }
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_USERAGENT, "Front-end Curl Agent");

        // set option for JSON call
        if ($convert)
        {

            curl_setopt($ch, CURLOPT_HTTPHEADER,
                    array(
                'Content-Type: application/json',
                'Content-Length:' . strlen($json_data),
                'authorized_user:' . $auth['username'],
                'authorized_token:' . $auth['token']
            ));
        }

        // Send the request & save response to $resp
        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Close request to clear up some resources
        curl_close($ch);

        return $this->compute_response($httpcode, $response);
    }

    /**
     * Execute PUT
     *
     * @param type $url
     *            URL to call
     * @param type $data
     *            array data
     * @param type $credentials
     *            array credentials
     *
     * @return type ARRAY. Described as following
     *         array ( 'httpcode' => HTTP Response code,
     *         'response' => HTTP Response body
     *         );
     */
    public function execute_put($url, $data = null, $credentials = array())
    {

        // check input
        if (!$this->is_valid_url($url))
        {
            return;
        }

        // check authentication credentials
        $auth = $this->check_auth($credentials);

        $json_data = null;

        // check data
        if ($data != null)
        {

            // encode data in json format
            $json_data = json_encode($data);

            $this->logger->log_debug("JSON data [" . $json_data . "]");
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, "Front-end Curl Agent");
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
                array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($json_data),
            'authorized_user:' . $auth['username'],
            'authorized_token:' . $auth['token']
        ));

        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        return $this->compute_response($httpcode, $response);
    }

    /**
     * Execute DELETE
     *
     * @param type $url
     *            URL to call
     * @param type $credentials
     *
     * @return type ARRAY. Described as following
     *         array ( 'httpcode' => HTTP Response code,
     *         'response' => HTTP Response body
     *         );
     */
    public function execute_delete($url, $credentials = array())
    {

        // check input
        if (!$this->is_valid_url($url))
        {
            return;
        }

        // check authentication credentials
        $auth = $this->check_auth($credentials);

        $ch = curl_init();

        curl_setopt_array($ch,
                array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => "Front-end Curl Agent",
            CURLOPT_HTTPHEADER => array(
                'authorized_user:' . $auth['username'],
                'authorized_token:' . $auth['token']
            )
        ));

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, "Front-end Curl Agent");
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

        // Send the request & save response to $resp
        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Close request to clear up some resources
        curl_close($ch);

        return $this->compute_response($httpcode, $response);
    }

}
