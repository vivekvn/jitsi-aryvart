<?php

/**
 * INTELLIGENCE LTD ("COMPANY") CONFIDENTIAL Unpublished Copyright (c) 2016 INTELLIGENCE, All Rights
 * Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of COMPANY. The
 * intellectual and technical concepts contained herein are proprietary to COMPANY and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from COMPANY. Access to the source code
 * contained herein is hereby forbidden to anyone except current COMPANY employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering
 * such access.
 * 
 * The copyright notice above does not evidence any actual or intended publication or disclosure of
 * this source code, which includes information that is confidential and/or proprietary, and is a
 * trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR
 * PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF
 * COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.
 * THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY
 * ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
defined('APP_PATH') || exit('No direct script access allowed');

require_once('Int_Log.php');

require_once(APP_PATH . '/php/common/Constants.php');
require_once(APP_PATH . '/php/util/StringUtil.php');
require_once(APP_PATH . '/php/core/Int_Session_Handler.php');

/**
 * 
 *
 * @author Giorgio Desideri - giorgio@intelligence.in.th
 */
class Int_Session
{

    /**
     *
     * @var Int_Log 
     */
    private $logger = NULL;

    /**
     *
     * @var type 
     */
    private static $session_id = NULL;
    private static $instance = NULL;

    const SESSION_STARTED = TRUE;
    const SESSION_NOT_STARTED = FALSE;

    // The state of the session
    private $sessionState = self::SESSION_NOT_STARTED;

    /**
     * Constructor.
     */
    function __construct($token = NULL)
    {
        // logger
        $this->logger = new Int_Log(strtoupper("Int_Session"));

        $handler = new Int_Session_Handler();
        session_set_save_handler($handler, true);

        // check token, randomize if NULL
        // session id
        if ($token != NULL)
        {
            // set session id
            self::$session_id = $token;
            session_id(self::$session_id);
        }

//        session_set_cookie_params(10);
    }

    /**
     * 
     */
    function __destruct()
    {
        $this->logger = NULL;
        self::$session_id = NULL;
    }

    public static function getInstance($token = NULL)
    {
        if (!isset(self::$instance))
        {
            self::$instance = new self($token);
        }

        return self::$instance;
    }

    function regenerate($id)
    {
        self::flush();
        ini_set('session.use_strict_mode', 0);
        session_id($id);
        self::$session_id = $id;
        self::start();
        ini_set('session.use_strict_mode', 1);
    }

    function start()
    {
        if ($this->sessionState == self::SESSION_NOT_STARTED)
        {
            $this->sessionState = session_start();
        }

        return $this->sessionState;
    }

    public function check_user_logged()
    {
        $data = $_SESSION[Constants::USER_DATA_SESSION_KEY];

        if (empty($data))
        {
            return FALSE;
        } else
        {
            return TRUE;
        }
    }

    /**
     * Get from session.
     * 
     * @param string $token
     * @param string $key
     * 
     * @return string value of key in session.
     */
    public function get($key)
    {
        if (isset($_SESSION[$key]))
        {
            return $_SESSION[$key];
        } else
        {
            return NULL;
        }
    }

    /**
     * Set value in session.
     * 
     * @param string $token
     * @param string $key
     * @param string $value
     * 
     * @return boolean TRUE if all is good, otherwise FALSE.
     */
    public function set($key, $value)
    {
        $_SESSION[$key] = $value;

        return TRUE;
    }

    /**
     * Remove session value from session.
     * 
     * @param type $token
     * @param type $key
     * 
     * @return TRUE is operation go in success, otherwise FALSE
     */
    public function remove($key)
    {
        session_unregister($key);

        return TRUE;
    }

    public function _isset($key)
    {
        return isset($_SESSION[$key]);
    }

    /**
     * 
     * @param type $token
     * @return type
     */
    public function flush()
    {
        if ($this->sessionState == self::SESSION_STARTED)
        {
            $this->sessionState = !session_destroy();

            return !$this->sessionState;
        }

        return FALSE;
    }

}
