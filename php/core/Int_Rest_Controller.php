<?php

/**
 * INTELLIGENCE LTD ("COMPANY") CONFIDENTIAL Unpublished Copyright (c) 2016 INTELLIGENCE, All Rights
 * Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of COMPANY. The
 * intellectual and technical concepts contained herein are proprietary to COMPANY and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from COMPANY. Access to the source code
 * contained herein is hereby forbidden to anyone except current COMPANY employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering
 * such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or disclosure of
 * this source code, which includes information that is confidential and/or proprietary, and is a
 * trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR
 * PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF
 * COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.
 * THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY
 * ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
defined('APP_PATH') || exit('No direct script access allowed');

require_once('Int_Log.php');
require_once('Int_Config.php');
require_once('Int_Server_Client.php');
require_once('Int_Session.php');

require_once(APP_PATH . '/php/common/Constants.php');
require_once(APP_PATH . '/php/common/HTTP_Constants.php');

require_once(APP_PATH . '/php/util/Security.php');
require_once(APP_PATH . '/php/util/Serializer.php');
require_once(APP_PATH . '/php/util/StringUtil.php');
require_once(APP_PATH . '/php/util/RandomUtil.php');

/**
 *
 * @author Giorgio Desideri <giorgio@intelligence.in.th>
 *
 *         Base REST controller.
 */
abstract class Int_Rest_Controller
{

    private $http_codes = array(
        HTTP_Constants::HTTP_CONTINUE => 'Continue',
        HTTP_Constants::HTTP_SWITCHING_PROTOCOLS => 'Switching Protocols',
        HTTP_Constants::HTTP_PROCESSING => 'Processing',
        //
        HTTP_Constants::HTTP_OK => 'OK',
        HTTP_Constants::HTTP_CREATED => 'Created',
        HTTP_Constants::HTTP_ACCEPTED => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',
        //
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Switch Proxy',
        307 => 'Temporary Redirect',
        // Client
        HTTP_Constants::HTTP_BAD_REQUEST => 'Bad Request',
        HTTP_Constants::HTTP_UNAUTHORIZED => 'Unauthorized',
        HTTP_Constants::HTTP_PAYMENT_REQUIRED => 'Payment Required',
        HTTP_Constants::HTTP_FORBIDDEN => 'Forbidden',
        HTTP_Constants::HTTP_NOT_FOUND => 'Not Found',
        HTTP_Constants::HTTP_METHOD_NOT_ALLOWED => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        449 => 'Retry With',
        450 => 'Blocked by Windows Parental Controls',
        // Server
        HTTP_Constants::HTTP_INTERNAL_SERVER_ERROR => 'Internal Server Error',
        HTTP_Constants::HTTP_NOT_IMPLEMENTED => 'Not Implemented',
        HTTP_Constants::HTTP_BAD_GATEWAY => 'Bad Gateway',
        HTTP_Constants::HTTP_SERVICE_UNAVAILABLE => 'Service Unavailable',
        HTTP_Constants::HTTP_GATEWAY_TIMEOUT => 'Gateway Timeout',
        HTTP_Constants::HTTP_VERSION_NOT_SUPPORTED => 'HTTP Version Not Supported',
        HTTP_Constants::HTTP_VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL => 'Variant Also Negotiates',
        HTTP_Constants::HTTP_INSUFFICIENT_STORAGE => 'Insufficient Storage',
        509 => 'Bandwidth Limit Exceeded',
        HTTP_Constants::HTTP_NOT_EXTENDED => 'Not Extended'
    );

    /**
     * Logger class
     * 
     * @var Int_Log
     */
    private $logger = NULL;

    /**
     * Back-End URL base prefix.
     * 
     * @var string 
     */
    private $server_url_prefix = NULL;

    /**
     * Request
     * 
     * @var \stdClass 
     */
    protected $request = NULL;

    /**
     * Response
     * 
     * @var \stdClass
     */
    protected $response = NULL;

    /**
     * Response format.
     * 
     * @var string 
     */
    protected $format = NULL;

    /**
     * Session.
     * 
     * @var Int_Session 
     */
    protected $session = NULL;

    /**
     * User token.
     * 
     * @var string
     */
    protected $user_token = NULL;

    /**
     * Constructor.
     * 
     * @param \stdClass $request
     * @param \stdClass $response
     * @param string $format
     */
    public function __construct($request, $response, $format = 'application/json')
    {
        // request
        $this->request = $request;

        // response
        $this->response = $response;

        // format
        $this->format = $format;

        // check cookie inside header
        if (array_key_exists(Constants::INT_FE_HEADER_NAME, $this->request->headers))
        {
            // session
            $this->user_token = $this->request->headers[Constants::INT_FE_HEADER_NAME];
            $this->session = Int_Session::getInstance($this->user_token);
        } else
        {
            // session
//            $this->user_token = RandomUtil::get_random_code(64);
            $this->session = Int_Session::getInstance();
        }
        $this->session->start();
//        $this->get_logger()->log_debug(">>>>> rest SID", session_id());

    }

    /**
     * Destructor
     */
    public function __destruct()
    {
        // Get the current timestamp
        //$this->end_time = microtime(TRUE);
    }

    /**
     * Send response
     * 
     * @param array $data
     * @param int $http_code
     */
    protected function response($data = NULL, $http_code = NULL)
    {
        // If the HTTP status is not NULL, then cast as an integer
        if ($http_code !== NULL)
        {
            // So as to be safe later on in the process
            $http_code = (int) $http_code;
        } else
        {
            $http_code = HTTP_Constants::HTTP_NOT_FOUND;
        }

        $this->get_logger()->log_trace("Set Response status: ", $http_code);

        // response status
        $this->set_response_status($http_code);

        $this->get_logger()->log_trace("Response format: ", $this->format);
        $this->get_logger()->log_trace("Response raw data: ", $data);

        // get response
        Serializer::format($this->response, $this->format, $data);

        $this->get_logger()->log_trace("Response: ", $this->response);

        // write        
        echo $this->response->body;

        exit;
    }

    /**
     * 
     * @param int $http_code
     */
    protected function set_response_status($http_code)
    {
        $protocol = $_SERVER['SERVER_PROTOCOL'] ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.1';

        $text = ' ' . $this->http_codes[$http_code];

        header($protocol . ' ' . $http_code . ' ' . $text, TRUE, $http_code);
    }

    /**
     * Gets parameter of GET request.
     * 
     * @param string $param_name
     * 
     * @return mixed
     */
    protected function get_param($param_name = '', $is_image = FALSE)
    {

        $this->get_logger()->log_debug("Media Request Response: ", $this->request->params);
        $security = new Security();

        if (array_key_exists($param_name, $this->request->params))
        {
            return $security->xss_clean($this->request->params[$param_name], $is_image);
        } else
        {
            return NULL;
        }
    }

    /**
     * Get logger instance
     * 
     * @return Int_Log
     */
    protected function get_logger()
    {
        if ($this->logger == NULL)
        {
            $class_name = get_called_class();

            // check
            if (StringUtil::is_empty($class_name))
            {
                $class_name = "Child_of_INT_Rest_Controller";
            }

            $this->logger = new Int_Log(strtoupper($class_name));
        }

        return $this->logger;
    }

    // -==========================================================================================-
    // 
    // SESSION MANAGEMENT
    //     
    // -==========================================================================================-

    /**
     * Retrieve object from session.
     * 
     * @param string $key session key
     * 
     * @return mixed
     */
    protected function session_get($key)
    {
        // get from session
        return $this->session->get($key);
    }

    /**
     * Save in session. If value already exist, it will override.
     * 
     * @param string $key
     * @param array $value
     */
    protected function session_put($key, $value)
    {
        // get from session
        return $this->session->set($key, $value);
    }

    /**
     * Remove from session the parameters.
     * 
     * @param string $key
     * 
     * @return boolean TRUE|FALSE
     */
    protected function session_remove($key)
    {
        // get from session
        return $this->session->set($key, $value);
    }

    /**
     * Flush partition
     * 
     * @return type
     */
    protected function session_flush()
    {
        // flush session
        $this->session->flush();
    }

    protected function session_regenerate($id)
    {
        // flush session
        $this->session->regenerate($id);
    }

    // -==========================================================================================-
    // 
    // BACK-END MANAGEMENT
    //
    // -==========================================================================================-

    /**
     * Get server URL prefix.
     * 
     * @param string $suffix url suffix
     * 
     * @return string URL
     */
    protected function get_server_url_prefix($suffix)
    {
        // check
        if ($this->server_url_prefix == NULL)
        {
            // check
            $config = Int_Config::get_instance();

            // assign
            $this->server_url_prefix = $config->get_config("server_url_url");

            // checl
            if (StringUtil::is_empty($this->server_url_prefix))
            {

                // assign as localhost
                $this->server_url_prefix = 'http://localhost/';
            }
        }

        return $this->server_url_prefix . trim($suffix);
    }

    /**
     * Get user credentials.
     * 
     * @return array user credentials as
     * array('username' => username, 'userToken' => userToken)
     */
    protected function get_user_credentials()
    {
        $user_data = $this->session->get(Constants::USER_DATA_SESSION_KEY);

        $this->get_logger()->log_debug("User data", $user_data);
        // data is null
        if ($user_data == NULL)
        {
            return NULL;
        }

        // prepare array
        return array(
            Constants::USER_NAME_KEY => $user_data[Constants::USER_NAME_KEY],
            Constants::TOKEN_KEY => $user_data[Constants::USER_TOKEN_KEY]
        );
    }

    /**
     * Call Back-End server function.
     * 
     * @param string $url_suffix
     * @param array $data
     * @param string $method
     * @param boolean $force_credentials
     * 
     * @return array server response
     */
    protected function call_backend_server($url_suffix, $data, $method = 'GET',
            $force_credentials = TRUE)
    {
        // forge the url
        $url = $this->get_server_url_prefix($url_suffix);

        $this->get_logger()->log_debug("URL to call: " . $url);

        // Commented by Xiang!!
//        if ($force_credentials)
//        {
//            // get user credentials
//            $credentials = $this->get_user_credentials();
//
//            // need credentials ?
//            if ($credentials == NULL)
//            {
//                $this->get_logger()->log_warn("Credentials is null");
//
//                return array(
//                    Constants::HTTP_CODE_KEY_NAME => HTTP_Constants::HTTP_FORBIDDEN);
//            }
//        } else
//        {
//            $credentials = NULL;
//        }
        $credentials = NULL;

        // create int server client
        $client = new Int_Server_Client();

        // check
        switch ($method) {
            case "POST":
                $this->logger->log_info("Execute POST");
                $response = $client->execute_post($url, $data, $credentials);
                break;

            case "PUT":
                $this->logger->log_info("Execute PUT");
                $response = $client->execute_put($url, $data, $credentials);
                break;

            case "DELETE":
                $this->logger->log_info("Execute DELETE");
                $response = $client->execute_delete($url, $credentials);
                break;

            case "GET":
                $this->logger->log_info("Execute GET");
                $response = $client->execute_get($url, $credentials);
                break;

            default:
                $this->logger->log_info("Execute NOTHING. Return NULL response");
                $response = NULL;
                break;
        }

        return $response;
    }

    /**
     * Parse server response.
     * 
     * @param array $response
     */
    protected function parse_server_response($response)
    {
        // response
        if ($response == NULL)
        {
            $this->response(HTTP_Constants::HTTP_NOT_IMPLEMENTED);
        }

        // code
        $resp_code = $response[Constants::HTTP_CODE_KEY_NAME];

        // message
        $resp_msg = $response[Constants::HTTP_RESULT_KEY_NAME];

        // check
        if (StringUtil::is_empty($resp_msg))
        {
            $this->get_logger()->log_trace("Response Code:", $resp_code);

            // reply with code only
            $this->response($resp_code);
        } else
        {
            $this->get_logger()->log_trace("Response Code:", $resp_code);
            $this->get_logger()->log_trace("Response MSG:", $resp_msg);

            // reply with data
            $this->response($resp_msg, $resp_code);
        }
    }

    /**
     * Parse server response ADDED BY XIANG!!!!!!!!!!!!!!!!!!!!!!!.
     * 
     * @param array $response
     */
    protected function parse_response($response)
    {
        // response
        if (empty($response))
        {
            $this->response(NULL, HTTP_Constants::HTTP_INTERNAL_SERVER_ERROR);
        }

        // code
        $resp_code = 500;
        $resp_msg = 'No message';
        if (array_key_exists(Constants::HTTP_CODE_KEY_NAME, $response))
        {
            if (!empty($response[Constants::HTTP_CODE_KEY_NAME]))
            {
                $resp_code = $response[Constants::HTTP_CODE_KEY_NAME];
            }
        }

        // message
        if (array_key_exists(Constants::HTTP_RESULT_KEY_NAME, $response))
        {
            $resp_msg = $response[Constants::HTTP_RESULT_KEY_NAME];
        }

        $protocol = $_SERVER['SERVER_PROTOCOL'] ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.1';
        header($protocol . ' ' . $resp_code . ' ' . $this->http_codes[$resp_code], TRUE, $resp_code);
        header(HTTP_Constants::HTTP_HEADER_CONTENT_TYPE . ': ' . $this->format, TRUE);
        header(HTTP_Constants::HTTP_HEADER_CONTENT_LENGHT . ': ' . strlen($resp_msg), TRUE);

        // Log
        $this->get_logger()->log_trace("RESPONSE code:", $resp_code);
        $this->get_logger()->log_trace("RESPONSE format:", $this->format);
//        $this->get_logger()->log_trace("RESPONSE message:", $resp_msg);

        echo $resp_msg;
        exit();
    }

    /**
     * Retrieve user data from session.
     * 
     * @param boolean $json_decode
     * 
     * @return string|array
     */
    protected function get_user_data($json_decode = FALSE)
    {
        // user data as json
        return $this->session->get(Constants::USER_DATA_SESSION_KEY);
    }

    /**
     * Update user data in session.
     * 
     * @param type $key
     * @param type $value
     * 
     * @return boolean TRUE|FALSE
     */
    protected function set_user_data($key, $value)
    {
        $user_data = $this->get_user_data(FALSE);

        // data
        if ($user_data == NULL) {
            $this->logger->log_info("User data is NULL");
            return FALSE;
        }

        // json decode
        //$user_data = json_decode($data, TRUE);

        $this->logger->log_debug("Update user data. Key: ", $key);
        $this->logger->log_debug("Update user data. Value: ", $value);

        // check key
        if (array_key_exists($key, $user_data))
        {
            $this->logger->log_debug("Update user data. Before: ", $user_data);

            $user_data[$key] = $value;

            $this->logger->log_debug("Update user data. After: ", $user_data);

            // update user data in session
            $this->session_put(Constants::USER_DATA_SESSION_KEY, $user_data);

            $this->logger->log_debug("Update user data. Session Updated");

            return TRUE;
        } else
        {
            return FALSE;
        }
    }

}
