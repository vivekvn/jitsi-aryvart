<?php

/**
 * INTELLIGENCE LTD ("COMPANY") CONFIDENTIAL Unpublished Copyright (c) 2016 INTELLIGENCE, All Rights
 * Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of COMPANY. The
 * intellectual and technical concepts contained herein are proprietary to COMPANY and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from COMPANY. Access to the source code
 * contained herein is hereby forbidden to anyone except current COMPANY employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering
 * such access.
 * 
 * The copyright notice above does not evidence any actual or intended publication or disclosure of
 * this source code, which includes information that is confidential and/or proprietary, and is a
 * trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR
 * PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF
 * COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.
 * THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY
 * ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
defined('APP_PATH') || exit('No direct script access allowed');

/**
 * Description of Int_Config
 *
 * @author Giorgio Desideri - giorgio@intelligence.in.th
 */
class Int_Config
{

    private static $instance = NULL;

    /**
     * Application configuration
     * 
     * @var array
     */
    private $app_configuration = array();

    /**
     * Routing table.
     * 
     * @var array
     */
    private $routing_table = array();

    /**
     * Constructor
     */
    private function __construct()
    {
        // check
        if (!is_dir(APP_PATH) || !file_exists(APP_PATH))
        {
            echo 'Application folder seems not configured properly. Check : ' . pathinfo(__FILE__,
              PATHINFO_BASENAME);
            exit(3); // EXIT_CONFIG
        }

        // parse CONFIG configuration
        $this->parse_configuration(APP_PATH . '/php/config/config.php');
        
        // parse LOGGING configuration
        $this->parse_configuration(APP_PATH . '/php/config/logging.php');
    }

    private function __clone()
    {
        // keep here.
    }

    private function __wakeup()
    {
        throw new Exception("Cannot unserialize singleton");
    }

    /**
     * Parse config file
     */
    private function parse_configuration($file_path)
    {
        $found = FALSE;

        // check
        if (file_exists($file_path))
        {
            $found = TRUE;
            require($file_path);
        }

        // check
        if (!$found)
        {
            echo 'The configuration file does not exist.';
            exit(3); // EXIT_CONFIG
        }

        // Does the $this->config array exist in the file?
        if (!isset($config) OR ! is_array($config))
        {
            echo 'Your config file does not appear to be formatted correctly.';
            exit(3); // EXIT_CONFIG
        }

        // Copy configuration
        foreach ($config as $key => $value) {
            $this->app_configuration[$key] = $value;
        }
    }

    /**
     * This instance.
     * 
     * @return Int_Config
     */
    public static function get_instance()
    {
        // check
        if (static::$instance == NULL)
        {
            static::$instance = new Int_Config();
        }

        return static::$instance;
    }

    /**
     * Get configuration value.
     * 
     * @param string $key
     * 
     * @return mixed
     */
    public function get_config($key)
    {
        // check array
        if (array_key_exists($key, $this->app_configuration))
        {
            return $this->app_configuration[$key];
        } else
        {
            return NULL;
        }
    }

    /**
     * Get routing table.
     */
    public function get_routing_table()
    {
        if (!empty($this->routing_table))
        {
            return $this->routing_table;
        }

        $rt_file = $this->get_config("route_config_file");

        // file not exist
        if (!file_exists(APP_PATH . $rt_file))
        {
            return array();
        }

        require_once(APP_PATH . $rt_file);

        // Copy configuration
        foreach ($routes as $key => $value) {
            $this->routing_table[$key] = $value;
        }

        return $this->routing_table;
    }

}
