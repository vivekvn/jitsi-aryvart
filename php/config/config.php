<?php

/**
 * INTELLIGENCE LTD ("COMPANY") CONFIDENTIAL Unpublished Copyright (c) 2016 INTELLIGENCE, All Rights
 * Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of COMPANY. The
 * intellectual and technical concepts contained herein are proprietary to COMPANY and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from COMPANY. Access to the source code
 * contained herein is hereby forbidden to anyone except current COMPANY employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering
 * such access.
 * 
 * The copyright notice above does not evidence any actual or intended publication or disclosure of
 * this source code, which includes information that is confidential and/or proprietary, and is a
 * trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR
 * PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF
 * COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.
 * THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY
 * ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
/**
 * ==========================================================
 * APPLICATION Configuration
 * 
 */
// keep final trailing slash
$config['controller_dir'] = '/php/controllers/';

// base server url, keep final trailing slash
// Server 
$config['server_url_url'] = 'http://192.168.1.51:8080/int-web-api/rest/';
// Giorgio
//$config['server_url_url'] = 'http://192.168.1.102:8080/int-web-api/rest/';
// Yut
//$config['server_url_url'] = 'http://192.168.1.103:8080/int-web-api/rest/';

/**
 * ==========================================================
 * ROUTING Configuration
 * 
 * route_config_file
 */
/**
 * Relative (based on application root directory) path of routing table file.
 */
$config['route_config_file'] = '/php/config/routes.php';


/**
 * ==========================================================
 * MEMCACHED Configuration
 * 
 * memcached_type
 * memcached_exire_time
 * memcached_compression
 * memcached_servers
 */
// client type LINUX or WINDOW$
if (strtoupper(php_uname("s")) === 'LINUX')
{
    $config['memcached_type'] = 'LINUX';
} else
{
    $config['memcached_type'] = 'WINDOW$';
}

// expiration value (in seconds)
$config['memcached_exire_time'] = 86400;

// compression
$config['memcached_compression'] = FALSE;

$config['memcached_servers'] = [
    'default' => [
        'host' => '192.168.1.51',
        'port' => '11211',
        'weight' => '1',
        'persistent' => FALSE
        ]];

/**
 * ==========================================================
 * PUBLIC URLs
 * 
 * public_urls
 */
$config['public_urls'] = [
    'list/*',
    'lang/*',
    'auth/*',
    'public/*'
];
