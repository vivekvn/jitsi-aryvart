<?php


/**
 * AUTHENTICATION URLs
 */
$routes['auth/session'] = array(
    'class' => 'auth/AuthenticationController',
    'class_method' => 'user_session_get'
);

$routes['auth/login'] = array(
    'method' => 'POST',
    'class' => 'auth/AuthenticationController',
    'class_method' => 'login',
);

$routes['auth/logout'] = array(
    'method' => 'POST',
    'class' => 'auth/AuthenticationController',
    'class_method' => 'logout',
);

$routes['list/preload/([A-Z_]+)/([A-Z]+)/static'] = array(
    'class' => 'static_list/StaticListRestController',
    'class_method' => 'list_preload',
    'param_names' => array('language', 'currency')
);