<?php

/**
 * INTELLIGENCE LTD ("COMPANY") CONFIDENTIAL Unpublished Copyright (c) 2016 INTELLIGENCE, All Rights
 * Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of COMPANY. The
 * intellectual and technical concepts contained herein are proprietary to COMPANY and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from COMPANY. Access to the source code
 * contained herein is hereby forbidden to anyone except current COMPANY employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering
 * such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or disclosure of
 * this source code, which includes information that is confidential and/or proprietary, and is a
 * trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR
 * PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF
 * COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.
 * THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY
 * ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
defined('APP_PATH') || exit('No direct script access allowed');

require_once(APP_PATH . '/php/core/Int_Rest_Controller.php');
require_once(APP_PATH . '/php/common/HTTP_Constants.php');
require_once(APP_PATH . '/php/common/Constants.php');
require_once(APP_PATH . '/php/core/Int_Memcached.php');

/**
 * Description of Static_list_controller
 *
 * @author Aung
 */
class StaticListRestController extends Int_Rest_Controller {

    private $default_lang = 'EN_US';
    private $default_currency = 'USD';
    private $currency_list = [
        'USD' => ['local_label' => 'USD', 'prefix_unit' => '$', 'suffix_unit' => '', 'rate' => 1],
        'THB' => ['local_label' => 'บาท', 'prefix_unit' => '฿', 'suffix_unit' => '. -', 'rate' => 34.96]
    ];

    /**
     */
    function __construct($request, $response) {
        parent::__construct($request, $response);

        parent::get_logger();
    }

    /**
     * Check before inside memcached, if result is FALSE, 
     * perform a call to  back-end to retrieve the list results.
     *
     * @param string $type to search
     *
     * @return mixed HTTP Response stream.
     */
    private function load_list($type) {
        $client = new Int_Memcached();

        $result = $client->get(strtoupper($type));

        if ($result) {
            // return the JSON to page
            return $result;
        }

        // assembly the url
        $url = "list/$type/get";
        return $this->load_list_by_url($url);
    }

    /**
     * Perfom a call to back-end to retrieve list results.
     * 
     * @param string $url
     * 
     * @return mixed HTTP Response stream.
     */
    private function load_list_by_url($url) {
        // call
        $result = parent::call_backend_server($url, NULL, 'GET', FALSE);
        $res = json_decode($result[Constants::HTTP_RESULT_KEY_NAME], TRUE);
        return $res['items'];
    }

    /**
     * Load inspiration tags.
     *
     * @return NULL,
     */
    public function list_inspiration_get() {
        return $this->load_list("inspirationTag");
    }

    /**
     * Load inspiration tags.
     *
     * @return NULL,
     */
    public function list_catetagory_get() {
        return $this->load_list("inspirationCategory");
    }

    /**
     * Load timezones.
     *
     * @return NULL,
     */
    public function list_timezone_get() {
        return $this->load_list("timezone");
    }

    /**
     * Load currencies.
     *
     * @return type
     */
    public function list_currency_get() {
        return $this->load_list("currency");
    }

    /**
     * Load languages.
     *
     * @return type
     */
    public function list_language_get() {
        return $this->load_list("language");
    }

    /**
     * Load countries.
     *
     * @return type
     */
    public function list_country_get() {
        return $this->load_list("country");
    }

    /**
     * Load entity type.
     *
     * @return type
     */
    public function list_entity_get() {
        return $this->load_list("entity");
    }

    /**
     * Load cities by country code.
     *
     * @return NULL,
     */
    public function list_city_get() {
        if (!$this->get('country_code')) {
            // return the JSON to page
            return $this->response(
                            [
                        BaseRestController::CTRL_RESULT_KEY_NAME => "country code not valid."
                            ], REST_Controller::HTTP_BAD_REQUEST);
        }

        $code = $this->get('country_code');

        $url = $this->server_url_prefix . "list/city/$code/get";

        return $this->load_list_by_url($url);
    }

    private function timezone_preload() {
        $timezone = [
            'list' => $this->load_list("timezone"),
            'current' => parent::session_get(Constants::TIMEZONE)
        ];

        for ($i = 0; $i < sizeof($timezone['list']); $i++) {
            if ($timezone['list'][$i]['code'] == $timezone['current']) {
                $timezone['current'] = $timezone['list'][$i];
                return $timezone;
            }
        }
        $timezone['current'] = null;
        return $timezone;
    }

    private function currency_preload($current) {

//        $json = parent::session_get(Constants::CURRENCY_SESSION_KEY);
//
//        if ($json[Constants::HTTP_CODE_KEY_NAME] == 200)
//        {
//            $current = $json[Constants::HTTP_RESULT_KEY_NAME];
//        }
//        else {
//            $current = $this->default_currency;
//        }

        $currency = [
            'list' => $this->currency_list,
            'current' => $current
        ];

        if ($currency['current'] == null) {
            $currency['current'] = $this->default_currency;
        }

        return $currency;
    }

    private function language_preload($lang_current) {

//        $lang_json = parent::session_get(Constants::LANG_CODE_SESSION_KEY);
//        
//        if ($lang_json[Constants::HTTP_CODE_KEY_NAME] == 200)
//        {
//            $lang_current = $lang_json[Constants::HTTP_RESULT_KEY_NAME];
//        }
//        else {
//            $lang_current = 'EN_US';
//        }

        $language = [
            'list' => $this->load_list("language"),
            'current' => $lang_current
        ];

        for ($i = 0; $i < sizeof($language['list']); $i++) {
            if ($language['current'] == $language['list'][$i]['code']) {
                $language['current'] = $language['list'][$i];
                return $language;
            }
        }

        $language['current'] = $language['list'][1];
        return $language;
    }

    private function translate($lang, $group_key) {
        $url = 'language/' . $lang . '/' . $group_key . '/get';
        $result = parent::call_backend_server($url, NULL, 'GET', FALSE);

        // response
        if ($result[Constants::HTTP_CODE_KEY_NAME] == 200) {
            $messages = json_decode($result[Constants::HTTP_RESULT_KEY_NAME], TRUE);
            $res_data = [];
            for ($i = 0; $i < count($messages); $i++) {
                $res_data[$messages[$i]['messageCode']] = $messages[$i]['messageValue'];
            }
            return $res_data;
        } else {
            return null;
        }
    }

    public function list_preload() {
        $lang = parent::get_param('language');
        $cur = parent::get_param('currency');

        parent::session_put(Constants::LANG_CODE_SESSION_KEY, $lang);
        parent::session_put(Constants::CURRENCY_SESSION_KEY, $cur);

        $timezone = $this->timezone_preload();
        $currency = $this->currency_preload($cur);
        $language = $this->language_preload($lang);

        $response = ['timezone' => $timezone,
            'currency' => $currency,
            'language' => $language,
            'translate' => $this->translate($language['current']['code'], 'app')
        ];

        if (!$response['translate']) {
            return parent::response($response, HTTP_Constants::HTTP_INTERNAL_SERVER_ERROR);
        } else {
            $data = ['httpcode' => 200, 'response' => json_encode($response)];
            header(HTTP_Constants::HTTP_HEADER_CACHE_CONTROL . ': public, max-age=31536000', TRUE);
            header_remove("Pragma"); 
            parent::parse_response($data);
            exit();
        }
    }

}
