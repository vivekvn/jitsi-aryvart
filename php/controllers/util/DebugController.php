<?php

/**
 * INTELLIGENCE LTD ("COMPANY") CONFIDENTIAL Unpublished Copyright (c) 2016 INTELLIGENCE, All Rights
 * Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of COMPANY. The
 * intellectual and technical concepts contained herein are proprietary to COMPANY and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from COMPANY. Access to the source code
 * contained herein is hereby forbidden to anyone except current COMPANY employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering
 * such access.
 * 
 * The copyright notice above does not evidence any actual or intended publication or disclosure of
 * this source code, which includes information that is confidential and/or proprietary, and is a
 * trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR
 * PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF
 * COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.
 * THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY
 * ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
defined('APP_PATH') || exit('No direct script access allowed');

require_once(APP_PATH . '/php/core/Int_Rest_Controller.php');
require_once(APP_PATH . '/php/common/HTTP_Constants.php');
require_once(APP_PATH . '/php/common/Constants.php');

/**
 * Authentication management
 *
 * @author Giorgio Desideri - giorgio@intelligence.in.th
 */
class DebugController extends Int_Rest_Controller
{

    function __construct($request, $response)
    {
        parent::__construct($request, $response, 'text/plain');

        $this->get_logger();
    }

    /**
     * Login.
     * 
     * @return HTTP Response
     */
    public function debug_session()
    {
        // check localhost
        $whitelist = array(
          '127.0.0.1', '::1',
          '192.168.1.51', '192.168.1.100', '192.168.1.101', '192.168.1.102', '192.168.1.103'
        );

        if (!in_array($_SERVER['REMOTE_ADDR'], $whitelist))
        {
            // Set the response and exit
            return $this->response(HTTP_Constants::HTTP_FORBIDDEN);
        }

        // open buffer
        ob_start();

        // var dump
        var_dump(parent::get_user_data(FALSE));
        
        var_dump($this->session->partition_content());

        // append to log message string
        $str = ob_get_clean();

        $this->response($str, HTTP_Constants::HTTP_OK);
    }

}
