<?php

/**
 * INTELLIGENCE LTD ("COMPANY") CONFIDENTIAL Unpublished Copyright (c) 2016 INTELLIGENCE, All Rights
 * Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of COMPANY. The
 * intellectual and technical concepts contained herein are proprietary to COMPANY and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from COMPANY. Access to the source code
 * contained herein is hereby forbidden to anyone except current COMPANY employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering
 * such access.
 * 
 * The copyright notice above does not evidence any actual or intended publication or disclosure of
 * this source code, which includes information that is confidential and/or proprietary, and is a
 * trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR
 * PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF
 * COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.
 * THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY
 * ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
defined('APP_PATH') || exit('No direct script access allowed');

require_once(APP_PATH . '/php/core/Int_Rest_Controller.php');
require_once(APP_PATH . '/php/common/HTTP_Constants.php');
require_once(APP_PATH . '/php/common/Constants.php');
require_once(APP_PATH . '/php/util/StringUtil.php');

/**
 * Authentication management
 *
 * @author Giorgio Desideri - giorgio@intelligence.in.th
 */
class AuthenticationController extends Int_Rest_Controller
{

    /**
     * Constructor.
     * 
     * @param type $request
     * @param type $response
     */
    function __construct($request, $response)
    {
        parent::__construct($request, $response);

        $this->get_logger("AuthenticationController");
    }

    /**
     * Get User Session.
     *
     * @return HTTP Response
     */
    public function user_session_get()
    {
        $user_data = $this->session_get(Constants::USER_DATA_SESSION_KEY);

        if ($user_data != NULL)
        {
            $user = [
                'userType' => $user_data['userType'],
                'userPictureCode' => $user_data['profile']['userPictureCode'],
                Constants::USER_VACATION_MODE => $user_data['onVacationMode'],
                Constants::USER_PROFILE_COMPLETE_FLAG_KEY => $user_data['isProfileComplete'],
                Constants::USER_FORCE_CHANGE_PWD_FLAG_KEY => $user_data['changePwdRequired'],
                Constants::USER_NAME_KEY => $user_data['profile']['username'],
                Constants::USER_CATEGORY_COMPLETE_FLAG_KEY => $user_data['isCategoryComplete'],
            ];
        } else
        {
            $user = NULL;
        }

        return parent::response($user, HTTP_Constants::HTTP_OK);
    }

    /**
     * Login.
     */
    public function login()
    {

        // check params
        if (!$this->get_param("data"))
        {
            // Set the response and exit
            return $this->response(HTTP_Constants::HTTP_BAD_REQUEST);
        }

        // decode json
        $data = json_decode($this->get_param("data"), TRUE);

        // prepare login data
        $login_data = [
            Constants::USER_NAME_KEY => $data[Constants::USER_NAME_KEY],
            Constants::PASSWORD_KEY => base64_encode($data[Constants::PASSWORD_KEY])
        ];

        // result
        $auth_result = $this->call_backend_server("auth/login", $login_data, 'POST', FALSE);

        // login success
        if ($auth_result[Constants::HTTP_CODE_KEY_NAME] === 200)
        {

            if (!$this->check_recaptcha($data['g-recaptcha-response']))
            {
                // Set the response and exit
                return $this->response(HTTP_Constants::HTTP_UNAUTHORIZED);
            }

            // get data
            $user_data = json_decode($auth_result[Constants::HTTP_RESULT_KEY_NAME], TRUE);

            // set user token
            $this->user_token = $user_data[Constants::USER_TOKEN_KEY];

            // add to session
            parent::session_regenerate($user_data[Constants::USER_TOKEN_KEY]);
            parent::session_put(Constants::USER_DATA_SESSION_KEY, $user_data);
            parent::session_put(Constants::TIMEZONE, $user_data['timezoneCode']);
            parent::session_put('expiration', time() + Constants::SESSION_EXPIRATION_TIME);

            // page data
            $page_data = [
                // shadow user code
                Constants::USER_TOKEN_KEY => $user_data[Constants::USER_TOKEN_KEY],
                // user code
                Constants::USER_CODE_KEY => $user_data[Constants::USER_CODE_KEY],
                // user active             
                Constants::USER_ACTIVE => $user_data[Constants::USER_ACTIVE],
                // user vacation           
                Constants::USER_VACATION_MODE => $user_data[Constants::USER_VACATION_MODE],
                // complete user profile
                Constants::USER_PROFILE_COMPLETE_FLAG_KEY => $user_data[Constants::USER_PROFILE_COMPLETE_FLAG_KEY],
                // complete user categories
                Constants::USER_CATEGORY_COMPLETE_FLAG_KEY => $user_data[Constants::USER_CATEGORY_COMPLETE_FLAG_KEY],
                // force change pwd
                Constants::USER_FORCE_CHANGE_PWD_FLAG_KEY => $user_data[Constants::USER_FORCE_CHANGE_PWD_FLAG_KEY]
            ];

            // return the JSON to page
            return $this->response($page_data, HTTP_Constants::HTTP_OK);
        } else
        {
            return parent::parse_response($auth_result);
        }
    }

    /**
     * Ultra secret code.
     *
     * Copied by http://code.ciphertrick.com/2015/05/19/google-recaptcha-with-angularjs/
     */
    private function check_recaptcha($captcha)
    {
        // check params
        if (!$captcha)
        {
            // Set the response and exit
            return $this->response(HTTP_Constants::HTTP_BAD_REQUEST);
        }

        // Build post data to make request with fetch_file_contents
        $postdata = http_build_query(
                array(
                    'secret' => Constants::GOOGLE_RECAPTCHA_SECRET,
                    'response' => $captcha,
                    'remoteip' => $_SERVER['REMOTE_ADDR']
        ));

        // Build options for the post request
        $opts = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );

        // Create a stream this is required to make post request with fetch_file_contents
        $context = stream_context_create($opts);

        /* Send request to Googles siteVerify API */
        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify", false,
                $context);
        $response = json_decode($response, true);

        // check
        if ($response["success"] === false)
        {
            return FALSE;
        } else
        {
            return TRUE;
        }
    }

    /**
     * Logout
     */
    public function logout()
    {
        // get user credentials
        $data = parent::get_user_credentials();

        // result
        $result = $this->call_backend_server("auth/logout", $data, 'POST');
        parent::get_logger()->log_debug("Logout response", $result);

        // success
        if ($result[Constants::HTTP_CODE_KEY_NAME] === 200)
        {
            parent::session_flush();
            // redirect
            header("Location : /");
            return;
        }
    }

    /**
     * Get user logged.
     */
    public function user_logged()
    {

        // get user credentials
        $n_user_data = parent::get_user_data(FALSE);

        // need credentials ?
        if ($n_user_data == NULL)
        {
            parent::get_logger()->log_warn("Credentials is null");

            return array(
                Constants::HTTP_CODE_KEY_NAME => HTTP_Constants::HTTP_FORBIDDEN);
        }

        $user_data = json_decode($n_user_data, TRUE);

        // init session
        $session = new Int_Session();

        // check
        $response = $session->check_user_logged($user_data[Constants::USER_CODE_KEY],
                $this->user_token);

        // success
        if ($response['httpcode'] == '200')
        {
            // return the JSON to page
            return $this->response([
                        Constants::MESSAGE_KEY => 'TRUE'
                            ], HTTP_Constants::HTTP_OK);
        } else
        {
            return $this->response([
                        Constants::MESSAGE_KEY => 'FALSE'
                            ], HTTP_Constants::HTTP_OK);
        }
    }

    /**
     * user forgot password.
     */
    public function forgot_password()
    {
        // check params
        // check
        if (!parent::get_param("data"))
        {

            parent::get_logger()->log_debug("No parameter named [data] in request");

            // return the JSON to page
            parent::response(NULL, HTTP_Constants::HTTP_BAD_REQUEST);
        }

        // decode json
        $data = json_decode(parent::get_param("data"), TRUE);

        // prepare login data
        $forgot_password_data = [
            Constants::EMAIL_2_KEY => $data[Constants::EMAIL_2_KEY]
        ];

        // url
        $url = "user/password/forgot";

        // execute
        $result = parent::call_backend_server($url, $forgot_password_data, 'POST', FALSE);

        // success
        if ($result[Constants::HTTP_CODE_KEY_NAME] == 200)
        {
            parent::response([Constants::CTRL_RESULT_KEY_NAME =>
                json_decode($result[Constants::HTTP_RESULT_KEY_NAME])], HTTP_Constants::HTTP_OK);
        } else
        {
            parent::response(NULL, $result[Constants::HTTP_CODE_KEY_NAME]);
        }
    }

}
