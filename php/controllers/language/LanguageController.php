<?php

/**
 * INTELLIGENCE LTD ("COMPANY") CONFIDENTIAL Unpublished Copyright (c) 2016 INTELLIGENCE, All Rights
 * Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of COMPANY. The
 * intellectual and technical concepts contained herein are proprietary to COMPANY and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from COMPANY. Access to the source code
 * contained herein is hereby forbidden to anyone except current COMPANY employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering
 * such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or disclosure of
 * this source code, which includes information that is confidential and/or proprietary, and is a
 * trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR
 * PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF
 * COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.
 * THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY
 * ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
defined('APP_PATH') || exit('No direct script access allowed');

require_once(APP_PATH . '/php/core/Int_Rest_Controller.php');
require_once(APP_PATH . '/php/core/Int_Language.php');
require_once(APP_PATH . '/php/common/HTTP_Constants.php');
require_once(APP_PATH . '/php/common/Constants.php');
require_once(APP_PATH . '/php/util/StringUtil.php');

/**
 * Giorgio Desideri <giorgio@intelligence.in.th>
 *
 * Language controller
 */
class LanguageController extends Int_Rest_Controller
{

    private $default_lang_key = 'english';
    private $default_lang_code = 'EN_US';

    function __construct($request, $response)
    {
        parent::__construct($request, $response);

        parent::get_logger();

        // get language groups
//        $this->get_languages_groups();
    }

    /**
     * Get languages group, if 
     * 
     * @return mixed $result array
     */
    private function get_languages_groups()
    {
        // check
        $result = parent::session_get(Constants::MEMCACHED_LANGUAGE_KEY);

        if ($result == NULL)
        {
            parent::get_logger()->log_debug("Not found on memcached, load by file");

            require_once('language_groups.php');

            // set value in memory
            parent::session_set(Constants::MEMCACHED_LANGUAGE_KEY, $int_lang_group);

            // assign
            $result = $int_lang_group;
        }

        parent::get_logger()->log_trace("Languages groups: ", $result);

        return $result;
    }

    /**
     * Returns current language.
     *
     * @return string current language code
     */
    private function get_current_language()
    {
        // get from session
        $lang_code = parent::session_get(Constants::LANG_CODE_SESSION_KEY);

        if (!empty($lang_code))
        {
            return $lang_code;
        } else
        {
            return 'EN_US';
        }
    }

    /**
     * Return translated message
     *
     * @param string $msg_key
     *
     * @return string translated message value
     */
    private function load_message($msg_key)
    {
        // get language
        $langs = $this->get_current_language();

        // load file
        $lang_obj = Int_Language::get_instance();

        // load message
        return $lang_obj->get_message($langs[Constants::LANG_SESSION_KEY], $msg_key);
    }

    /**
     * Get current language
     */
    public function get_language()
    {
        parent::get_logger()->log_debug("Hello !!!!!!");

        $langs = $this->get_current_language();

        parent::get_logger()->log_debug("Hello !!!!!!");

        // return OK to caller
        return $this->response(
                        [
                    'language' => $langs[Constants::LANG_CODE_SESSION_KEY]
                        ], HTTP_Constants::HTTP_OK);
    }

    /**
     * Set language
     */
    public function set_language()
    {
        // get language code
        $lang_code = parent::get_param('lang_code');

        // check post code in get params
        if (!$lang_code)
        {
            return $this->response(NULL, HTTP_Constants::HTTP_BAD_REQUEST);
        }

        if (!StringUtil::is_empty($lang_code))
        {
            // set language code
            parent::session_put(Constants::LANG_CODE_SESSION_KEY, $lang_code);
        } 
        else
        {
            $lang_code = $this->default_lang_code;

            // default
            parent::session_put(Constants::LANG_CODE_SESSION_KEY, $lang_code);
        }

        // return OK to caller
        return $this->response($lang_code, HTTP_Constants::HTTP_OK);
    }

    /**
     * Return the message value translated according current language.
     */
    public function get_message()
    {
        // check post code in get params
        if (!parent::get_param('msg_key'))
        {
            return $this->response(NULL, HTTP_Constants::HTTP_BAD_REQUEST);
        }

        // message key
        $msg_key = parent::get_param('msg_key');

        // get message value requested
        $msg_value = $this->load_message($msg_key);

        // result
        $result = [
            $msg_key => $msg_value
        ];

        // return OK to caller
        return $this->response(
                        [
                    Constants::MESSAGE_KEY => json_encode($result)
                        ], HTTP_Constants::HTTP_OK);
    }

    /**
     * Return the messages values translated according current language.
     */
    public function get_group()
    {
        $group_key = parent::get_param('group_key');

        // check post code in get params
        if (!$group_key)
        {
            parent::response(NULL, HTTP_Constants::HTTP_BAD_REQUEST);
        }

        // get language - Xiang added
        $langs = $this->get_current_language();

        // call back end
        $url = 'language/' . $langs . '/' . $group_key . '/get';
        $result = parent::call_backend_server($url, NULL, 'GET', FALSE);

        // response
        if ($result[Constants::HTTP_CODE_KEY_NAME] == 200)
        {
            $messages = json_decode($result[Constants::HTTP_RESULT_KEY_NAME], TRUE);
            $res_data = [];
            for ($i = 0; $i < count($messages); $i++) {
                $res_data[$messages[$i]['messageCode']] = $messages[$i]['messageValue'];
            }
//            parent::get_logger()->log_trace("Languages group: ", $res_data);
            parent::response($res_data, HTTP_Constants::HTTP_OK);
        } else
        {
            parent::response(NULL, HTTP_Constants::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Translation with cache enable - Added by Xiang
     */
    public function get_translation()
    {
        $group_key = parent::get_param('group');
        $langs = parent::get_param('lang');

        // check post code in get params
        if (!$group_key || !$langs)
        {
            parent::response(NULL, HTTP_Constants::HTTP_BAD_REQUEST);
        }

        // get language - Xiang added
//        $langs = $this->get_current_language();

        // call back end
        $url = 'language/' . $langs . '/' . $group_key . '/get';
        $result = parent::call_backend_server($url, NULL, 'GET', FALSE);

        // response
        if ($result[Constants::HTTP_CODE_KEY_NAME] == 200)
        {
            $messages = json_decode($result[Constants::HTTP_RESULT_KEY_NAME], TRUE);
            $res_data = [];
            for ($i = 0; $i < count($messages); $i++) {
                $res_data[$messages[$i]['messageCode']] = $messages[$i]['messageValue'];
            }

            $result[Constants::HTTP_RESULT_KEY_NAME] = json_encode($res_data);
            header(HTTP_Constants::HTTP_HEADER_CACHE_CONTROL . ': public, max-age=31536000', TRUE);
            header_remove("Pragma"); 
        }

        parent::parse_response($result);
    }

    /**
     * 
     */
    public function get_language_info()
    {
        // load from memecached
        $languages = parent::session_get(strtoupper("language"));

        if (!isset($languages) || $languages === FALSE)
        {
            // call
            $result_ws = parent::call_backend_server("list/language/get", $languages, 'GET', FALSE);

            // success
            if ($result_ws[Constants::HTTP_CODE_KEY_NAME] === 200)
            {
                // JSON message
                $tmp_data = json_decode($result_ws[Constants::HTTP_RESULT_KEY_NAME], TRUE);

                // go to currencies
                $languages = $tmp_data["items"];
            }
        }

        // assembly data
        $data = array(
            'current_language' => $this->get_current_language(),
            'languages' => $languages
        );

        // return the JSON to page
        return $this->response([Constants::CTRL_RESULT_KEY_NAME => json_encode($data)],
                        HTTP_Constants::HTTP_OK);
    }

    /**
     * Upload Language CSV file
     */
    public function upload_lang_csv()
    {
        // get request data
        $data = parent::get_param('data');

        // check params
        if (!$data)
        {
            parent::response(NULL, HTTP_Constants::HTTP_BAD_REQUEST);
        }

        // add usercode data
        $lang_data = json_decode($data, TRUE);
        $lang_data[Constants::USER_CODE_KEY] = parent::get_user_data(TRUE)[Constants::USER_CODE_KEY];

        // call backend
        $response = parent::call_backend_server('language/save', $lang_data, "POST");

        parent::parse_response($response);
    }

}
