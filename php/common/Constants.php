<?php

/**
 * INTELLIGENCE LTD ("COMPANY") CONFIDENTIAL Unpublished Copyright (c) 2016 INTELLIGENCE, All Rights
 * Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of COMPANY. The
 * intellectual and technical concepts contained herein are proprietary to COMPANY and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from COMPANY. Access to the source code
 * contained herein is hereby forbidden to anyone except current COMPANY employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering
 * such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or disclosure of
 * this source code, which includes information that is confidential and/or proprietary, and is a
 * trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR
 * PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF
 * COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.
 * THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY
 * ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
defined('APP_PATH') || exit('No direct script access allowed');

/**
 * Class file for only variables.
 *
 * @author Giorgio Desideri - giorgio@intelligence.in.th
 *
 */
class Constants
{

    const GOOGLE_RECAPTCHA_SECRET = '6LcrbSkTAAAAALiVKJ0wD6dIhMQYZ7utlVK-SYNq';
    
    const INT_FE_HEADER_NAME = 'int-fe-shadow';

    const HTTP_CODE_KEY_NAME = 'httpcode';

    const HTTP_RESULT_KEY_NAME = 'response';

    const HTTP_STATUS_KEY_NAME = 'status';

    const CTRL_RESULT_KEY_NAME = 'result';

    const TOKEN_KEY = "token";

    const HTTP_HEADER_HTML_VERSION_1_0 = 'HTTP/1.0';

    const HTTP_HEADER_HTML_VERSION_1_1 = 'HTTP/1.1';

    const HTTP_HEADER_CONTENT_TYPE_HTML = 'text/html';
    
    const HTTP_HEADER_CONTENT_TYPE = 'Content-Type';
    
    const HTTP_HEADER_CONTENT_LENGHT = 'Content-Length';
    
    const HTTP_HEADER_CACHE_CONTROL = 'Cache-Control';
    
    const HTTP_HEADER_CACHE_PRAGMA = 'Pragma';
    
    const HTTP_HEADER_LAST_MODIFIED = 'Last-Modified';
    
    const HTTP_HEADER_CONTENT_DISPOSITION = 'Content-Disposition';
    
    const MEMCACHED_LANGUAGE_KEY = 'LANGUAGES_GROUP_ITEMS_123467890';

    const MEMCACHED_CHUNK_SIZE = 655360;
    
    const SESSION_EXPIRATION_TIME = 1800;
    
    const MEMCACHED_UPLOADED_FILE_EXP_TIME = 600;
    
    /*
     * USER variables
     */
    
    const USER_DATA_SESSION_KEY = "USER_DATA";
    
    const USER_TOKEN_KEY = "userToken";

    const USER_NAME_KEY = "username";

    const USER_CODE_KEY = "userCode";

    const USER_RELATIONS = "relations";

    const ENTITY_NAME_KEY = "entityName";

    const ENTITY_TYPE_KEY = "entityType";

    const SINGLE_USER_KEY = "singleUser";

    const PASSWORD_KEY = "password";

    const USER_AUTHORIZATION_KEY = "authorizationDatas";

    const USER_INFO_KEY = "userInfo";

    const PROFILE_DESCRIPTION_KEY = 'profileDescription';

    const PICTURE = 'picture';

    const PICTURE_MIMETYPE_KEY = "pictureMimeType";

    const TIMEZONE = 'timezone';

    const EMAIL_KEY = "contactEmail";
    
    const EMAIL_2_KEY = "email";

    const USER_PICTURE_CODE_KEY = 'pictureCode';

    const USER_PICTURE_SMALLER_CODE_KEY = 'pictureSmallerCode';

    const USER_PICTURE_SMALL_CODE_KEY = 'pictureSmallCode';

    const USER_PICTURE_MEDIUM_CODE_KEY = 'pictureMediumCode';

    const USER_CURRENCY_CODE_KEY = 'currencyCode';

    const USER_BIRTHDATE_KEY = 'birthdate';

    const USER_PROFILE_COMPLETE_FLAG_KEY = 'isProfileComplete';

    const USER_CATEGORY_COMPLETE_FLAG_KEY = 'isCategoryComplete';

    const USER_FORCE_CHANGE_PWD_FLAG_KEY = 'changePwdRequired';

    const COUNTRY_KEY = 'country';

    const CITY_KEY = 'city';

    const USER_SKILLS_KEY = 'tags';

    const USER_FIRST_NAME_KEY = 'firstName';

    const USER_LAST_NAME_KEY = 'lastName';

    const USER_FULL_NAME_KEY = 'contactFullName';
    
    const USER_VACATION_MODE = 'onVacationMode';
    
    const USER_VACATION_MODE_TIMESTAMP = 'vacationModeStartTime';
    
    const USER_ACTIVE = 'active';
    
    const USER_PAYOUT_INFO_FLAG_NAME = 'hasPayoutInformation';
    
    const USER_BILLING_INFO_FLAG_NAME = 'hasBillingInformation';

    /*
     * LANGUAGE KEYs
     */
    const LANG_SESSION_KEY = 'current_language';

    const LANG_CODE_SESSION_KEY = 'current_language_code';

    const LANG_FILE_NAME_KEY = 'messages';

    const CONFIG_DEFAULT_LANG_KEY = 'language';

    const CONFIG_DEFAULT_LANG_CODE_KEY = 'language_code';

    /*
     * UPLOAD KEYs
     */
    const UPLOAD_MEDIA_PARAM_NAME = 'file_upload';

    const UPLOAD_MEDIA_NAME = 'name';

    const UPLOAD_MEDIA_TYPE = 'type';

    const UPLOAD_MEDIA_FULL_PATH = 'full_path';

    const UPLOADED_MEDIAS_SESSION_KEY = 'uploaded_medias';
    
    const UPLOAD_DIR_LINUX = '/data/uploads/';
    
    const UPLOAD_DIR_WINDOWS = 'C:\xampp\tmp\shadow';
    
    /*
     * MONEY keys
     */
    const CURRENCY_SESSION_KEY = 'current_currency_code';

    const CURRENCY_AMOUNT_KEY = 'amount';

    const CONFIG_DEFAULT_CURRENCY_CODE_KEY = 'currency_default_code';

    /*
     * TAGS
     */
    const DEFAULT_CATEGORY_NO_KEY = '15';

    const SEARCH_CATEGORY_LIMIT_KEY = '50';

    const DEFAULT_TAG_NO_KEY = '15';

    const SEARCH_TAG_LIMIT_KEY = '50';

    /*
     * COMMON Keys
     */
    const FB_LOGIN_URL_KEY = "login_url";

    const ITEMS_KEY = "items";

    const MESSAGE_KEY = 'message';

    const ERROR_MSG_KEY = 'msg';

    const CODE_KEY = 'code';

    const PICTURE_BASE64_KEY = 'pictureBase64';

    const PRICE_KEY = 'price';

    const IS_PUBLIC_KEY = 'isPublic';

    const STATUS_KEY = 'status';

    const TITLE_KEY = 'title';

    const TAGLINE_KEY = 'tagline';

    const CURRENCY_CODE_KEY = 'currencyCode';

    const CATEOGRIES_KEY = 'categories';

    const SKILLS_KEY = 'skills';

    const LANGUAGES_KEY = 'languages';
    
    const DATA_KEY = 'data';

    /*
     * CATEGORY Keys
     */
    const CATEOGRIES_SESSION_KEY = 'categories';

    /*
     * EVENT Keys
     */
    const EVENT_INFO_SESSION_KEY = 'event_info';

    const EVENT_PICTURE_CODES_KEY = 'pictureKeys';

    const EVENT_SESSION_MEDIA_PARAM_KEY = 'event_session_media';

    const EVENT_CONTENT_KEY = 'content';

    const EVENT_AUDIENCE_KEY = 'audience';

    const EVENT_GOAL_KEY = 'goal';

    const EVENT_REQUIREMENT_KEY = 'requirements';

    const EVENT_MANAGER_USER_CODE_KEY = 'managerUserCode';

    const EVENT_OWNER_USER_CODE_KEY = 'ownerUserCode';

    const EVENT_SCHED_SESSION_KEY = 'scheduleSessions';
    
    const EVENT_SESSION_PARAM_KEY = 'event_session_data';
    
    const EVENT_SCHEDULE_SESSION_INDEX_PARAM_KEY = 'sched_sess_index';
    
    const EVENT_SESSION_INDEX_PARAM_KEY = 'sess_index';

    /*
     * SERVICE Keys by Arm
     */
    const SERVICE_CODE = 'code';
    
    const SERVICE_INFO_ADDON_KEY = 'service_info';

    const DESCRIPTION_KEY = 'description';

    const USER_CREATE_CODE_KEY = "creatorUserCode";

    const NUMBER_PER_DAY_KEY = "activeOrder";

    const DELIVERT_DURATION_KEY = "deliveryDuration";

    const PRESENTATIONS_KEY = "presentations";

    const SAMPLES_KEY = "samples";

    const ORDER_PER_DAY_KEY = "activeOrder";

    const ADD_ON_KEY = "addons";

    const PRESENT_IMAGE_KEY = "presentImages";

    const PRESENT_VIDEO_KEY = "presentVideo";
    
    /*
     * Facebook Register Keys by Aung
     */
    
    const FACEBOOK_ID = 'facebookId';
       
}
