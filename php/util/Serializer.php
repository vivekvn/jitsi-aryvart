<?php

/**
 * INTELLIGENCE LTD ("COMPANY") CONFIDENTIAL Unpublished Copyright (c) 2016 INTELLIGENCE, All Rights
 * Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of COMPANY. The
 * intellectual and technical concepts contained herein are proprietary to COMPANY and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from COMPANY. Access to the source code
 * contained herein is hereby forbidden to anyone except current COMPANY employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering
 * such access.
 * 
 * The copyright notice above does not evidence any actual or intended publication or disclosure of
 * this source code, which includes information that is confidential and/or proprietary, and is a
 * trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR
 * PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF
 * COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.
 * THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY
 * ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */
defined('APP_PATH') || exit('No direct script access allowed');

require_once('Security.php');
require_once('StringUtil.php');

require_once(APP_PATH . '/php/common/HTTP_Constants.php');

/**
 * Description of Serializer
 *
 * @author Giorgio Desideri - giorgio@intelligence.in.th
 */
class Serializer
{

    /**
     * 
     * @param type $format
     * @param type $data
     */
    public static function format(&$response, $format = 'application/json', $data = NULL)
    {

        if (($data == NULL) || empty($data))
        {
            // assign headers
            $response->headers = array(
              HTTP_Constants::HTTP_HEADER_CACHE_CONTROL => 'no-cache, must-revalidate',
              HTTP_Constants::HTTP_HEADER_CONTENT_TYPE => $format,
              HTTP_Constants::HTTP_HEADER_CONTENT_LENGHT => 0,
            );

            // assign body
            $response->body = NULL;
        }

        switch ($format) {

            case StringUtil::starts_with($format, "image"):
                // assign headers
                $response->headers = array(
                  HTTP_Constants::HTTP_HEADER_CACHE_CONTROL => 'no-cache, must-revalidate',
                  HTTP_Constants::HTTP_HEADER_CACHE_PRAGMA => 'no-cache',
                  HTTP_Constants::HTTP_HEADER_CONTENT_TYPE => $format,
                  HTTP_Constants::HTTP_HEADER_CONTENT_LENGHT => sizeof($data),
                );

                // assign body
                $response->body = $data;
                break;

            case 'text/html':
            case 'text/plain':
                // assign headers
                $response->headers = array(
                  HTTP_Constants::HTTP_HEADER_CACHE_CONTROL => 'no-cache, must-revalidate',
                  HTTP_Constants::HTTP_HEADER_CONTENT_TYPE => $format,
                  HTTP_Constants::HTTP_HEADER_CONTENT_LENGHT => strlen($data),
                );

                // assign body
                $response->body = $data;
                break;

            case 'application/json':
            default:

                $body = json_encode($data);

                // assign headers
                $response->headers = array(
                  HTTP_Constants::HTTP_HEADER_CACHE_CONTROL => 'no-cache, must-revalidate',
                  HTTP_Constants::HTTP_HEADER_CONTENT_TYPE => 'application/json',
                  HTTP_Constants::HTTP_HEADER_CONTENT_LENGHT => strlen($body),
                );

                // assign body
                $response->body = $body;
                break;
        }
    }

}
