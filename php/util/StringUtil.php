<?php

/**
 * INTELLIGENCE LTD ("COMPANY") CONFIDENTIAL Unpublished Copyright (c) 2016 INTELLIGENCE, All Rights
 * Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of COMPANY. The
 * intellectual and technical concepts contained herein are proprietary to COMPANY and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from COMPANY. Access to the source code
 * contained herein is hereby forbidden to anyone except current COMPANY employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering
 * such access.
 * 
 * The copyright notice above does not evidence any actual or intended publication or disclosure of
 * this source code, which includes information that is confidential and/or proprietary, and is a
 * trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR
 * PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF
 * COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.
 * THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY
 * ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */

/**
 * 
 */
class StringUtil
{

    /**
     * Check if string starts with pattern.
     * 
     * @param type $haystack input
     * @param type $needle pattern to check
     * 
     * @return boolean TRUE|FALSE
     */
    public static function starts_with($haystack, $needle)
    {
        $length = strlen($needle);

        return (substr($haystack, 0, $length) === $needle);
    }

    /**
     * Check if string ends with pattern.
     * 
     * @param type $haystack input
     * @param type $needle pattern to check
     * 
     * @return boolean TRUE|FALSE
     */
    public static function ends_with($haystack, $needle)
    {
        $length = strlen($needle);

        if ($length == 0)
        {
            return TRUE;
        }

        if (substr($needle, -1) === $haystack)
        {
            return TRUE;
        } else
        {
            return FALSE;
        }
    }

    /**
     * Check if string is empty
     * 
     * @param string $string
     * 
     * @return boolean TRUE|FALSE
     */
    public static function is_empty($string)
    {
        return (! isset($string) || $string == NULL || trim($string) === '');
    }
    
    /**
     * Check if string matches fully with pattern.

     * @param type $pattern
     * @param type $input 
     * 
     * @return boolean TRUE|FALSE
     */
    public static function str_match_full($pattern, $input)
    {
        // check input parameters
        if (! is_string($pattern) || ! is_string($input)) {
            return FALSE;
        }

        // check
        if (preg_match($pattern, $input, $matches)) {

            $full_match = FALSE;

            // iterate on matches
            foreach ($matches as $current) {

                // check if matches is equals to original string
                if (strcmp($current, $input) == 0) {
                    $full_match = TRUE;
                    break;
                }
            }

            return $full_match;
        } // no matches
        else {
            return FALSE;
        }
    }
}
