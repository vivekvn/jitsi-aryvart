<!--
 * @package AngularJS
 * @version 2
 * @File Type: PHP
 * @Author Gnanavel
 * @created date 22 Mar 2017
 * @Description  This is the api file which provides all the students
-->

<?php
header("Access-Control-Allow-Origin: *");
$students = '[{"firstname":"Gnanavel","lastname":"priyank","status":"Online"},
{"firstname":"Amit","lastname":"priyank","status":"Online"},
{"firstname":"Sathish","lastname":"priyank","status":"Online"},
{"firstname":"Jayavel","lastname":"priyank","status":"Online"},
{"firstname":"Clark","lastname":"priyank","status":"Offline"},
{"firstname":"Vincent","lastname":"priyank","status":"Offline"},
{"firstname":"Hogg","lastname":"priyank","status":"Online"},
{"firstname":"Williamson","lastname":"priyank","status":"Online"},
{"firstname":"David","lastname":"priyank","status":"Idle"},
{"firstname":"Warner","lastname":"priyank","status":"Offline"},
{"firstname":"Jack","lastname":"priyank","status":"Offline"},
{"firstname":"Michal","lastname":"priyank","status":"Idle"},
{"firstname":"Hussey","lastname":"priyank","status":"Offline"},
{"firstname":"Luke","lastname":"priyank","status":"Idle"},
{"firstname":"Dhoni","lastname":"priyank","status":"Offline"},
{"firstname":"Palani","lastname":"priyank","status":"Online"},
{"firstname":"Vinoth","lastname":"priyank","status":"Offline"},
{"firstname":"Rajesh","lastname":"priyank","status":"Idle"},
{"firstname":"Williamson","lastname":"priyank","status":"Online"}]';
echo $students;
?>