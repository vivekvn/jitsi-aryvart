/*
 * @package AngularJS
 * @version 2
 * @File Type: Javascript
 * @Author Gnanavel
 * @created date 8 Mar 2017
 * @Description  This is user controller file which contain functionalities for various actions related to all the users such as teacher,students
*/



app.controller('MainController', MainController);
  /** @ngInject */
  function MainController($timeout,$scope,$location,$http) {
    var vm = this;
	$scope.form = [];
	      $scope.files = [];

	      $scope.submit = function() {
	      	$scope.form.image = $scope.files[0];
			
			/* alert("Hello World"); */
	      	$http({
			  method  : 'POST',
			  url     : '/app/views/upload.php',
			  processData: false,
			  transformRequest: function (data) {
			      var formData = new FormData();
			      formData.append("image", $scope.form.image);  
			      return formData;  
			  },  
			  data : $scope.form,
			  headers: {
			         'Content-Type': undefined
			  }
		   }).success(function(data){
		        alert(data);
		   });

	      };

	      $scope.uploadedFile = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source = event.target.result
		      $scope.$apply(function($scope) {
		        $scope.files = element.files;
		      });
		    }
            reader.readAsDataURL(element.files[0]);
		  }

		 //updating the chat conversation between teacher and students 	

		$scope.chat = function() {
		          $http({
		              
		              method: 'POST',
		              url: 'https://jitsi.aryvarttechnologies.com/api/chat.php',
				      params: {
		    			    msg : angular.element(".chat-msg-box").val()
					  }
		              
		          }).then(function (response) {
		              
		              // on success
		              $scope.chat_message = response.data;
		              angular.element(".chat-msg-box").val("");
		              
		          }, function (response) {
		              
		              // on error
		              console.log(response.data,response.status);
		              
		          });
		    }

		//Getting list of active students
		$http.get("https://jitsi.aryvarttechnologies.com/api/initial_chat.php")   
		.then(function (response) {$scope.chat_message = response.data;});
		
		//Getting list of active students
		$http.get("https://jitsi.aryvarttechnologies.com/api/get_students.php")   
		.then(function (response) {$scope.students = response.data;});
		
		//Getting list of students who shared the files
		$http.get("https://jitsi.aryvarttechnologies.com/api/file_share_students.php")   
		   .then(function (response) {$scope.file_shared_students = response.data;});

		//Getting list of chat students 
		$http.get("https://jitsi.aryvarttechnologies.com/api/chat_students.php")   
		   .then(function (response) {$scope.chat_students = response.data;});

		//Getting even details
		$http.get("https://jitsi.aryvarttechnologies.com/api/get_event_details.php")   
		.then(function (response) {$scope.event_details = response.data;});   

	 	//It call the screen2 page
    	$scope.callPage2 = function(){
    		$location.path("/screen2");
    	}

      	//It call the screen3 page
    	$scope.teacher = function(){
    		$location.path("/screen3");
    	}

      	//It call the upload2 page
    	$scope.upload = function(){
    		$location.path("/upload2");
    	}

      	//It call the home page
        $scope.home = function(){
        	$location.path("/main");
        }

      	//It call the screen2 page
      	$scope.screen2 = function(){
        	$location.path("/screen2");
      	}

      	//It call the screen3 page
      	$scope.screen3 = function(){
        	$location.path("/screen3");
      	}
		
		
		
		
		
		  $('.modal').modal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: .5, // Opacity of modal background
      inDuration: 300, // Transition in duration
      outDuration: 200, // Transition out duration
      startingTop: '4%', // Starting top style attribute
      endingTop: '10%', // Ending top style attribute
      ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
        /* alert("Ready"); */
        console.log(modal, trigger);
      },
      complete: function() { /* alert('Closed'); */ } // Callback for Modal close
    }
  );

    

      	


  }
  
  

