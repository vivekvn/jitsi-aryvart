/*
 * @package AngularJS
 * @version 2
 * @File Type: Javascript
 * @Author Gnanavel
 * @created date 8 Mar 2017
 * @Description  This is main controller which contain functionalities for various actions
*/



app.controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout,$scope,$location,$http) {
    var vm = this;
  	
  	$scope.name = 'World';
    $scope.files = []; 
    $scope.upload=function(){
      alert($scope.files.length+" files selected ... Write your Upload Code"); 
      
    };

   	$('.button-collapse').sideNav();
    $('.parallax').parallax();
	
  	
  	$('.dropdown-buttons').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false, // Does not change width of dropdown to that of the activator
        hover: true, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: false, // Displays dropdown below the button
        alignment: 'left', // Displays dropdown with edge aligned to the left of button
        stopPropagation: false // Stops event propagation
      }
    );
      
    $('.dropdown-button1').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false, // Does not change width of dropdown to that of the activator
        hover: true, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: false, // Displays dropdown below the button
        alignment: 'left', // Displays dropdown with edge aligned to the left of button
        stopPropagation: false // Stops event propagation
      }
    );
  
    $('.dropdown-button2').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false, // Does not change width of dropdown to that of the activator
        hover: true, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: false, // Displays dropdown below the button
        alignment: 'left', // Displays dropdown with edge aligned to the left of button
        stopPropagation: false // Stops event propagation
      }
    );
  
    $(document).ready(function(){
      $('ul.tabs').tabs();
    });
    
  

  }


app.directive('ngFileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.ngFileModel);
            var isMultiple = attrs.multiple;
            var modelSetter = model.assign;
            element.bind('change', function () {
                var values = [];
                angular.forEach(element[0].files, function (item) {
                    var value = {
                       // File Name 
                        name: item.name,
                        //File Size 
                        size: item.size,
                        //File URL to view 
                        url: URL.createObjectURL(item),
                        // File Input Value 
                        _file: item
                    };
                    values.push(value);
                });
                scope.$apply(function () {
                    if (isMultiple) {
                        modelSetter(scope, values);
                    } else {
                        modelSetter(scope, values[0]);
                    }
                });
            });
        }
    };
}]);


















/*
 * @package AngularJS
 * @version 2
 * @File Type: Javascript
 * @Author Gnanavel
 * @created date 8 Mar 2017
 * @Description  This is specification file for main controller 
*/

(function() {
  'use strict';

  describe('controllers', function(){
    var vm;
    var $timeout;
    var toastr;

    beforeEach(module('Intelligence'));
    beforeEach(inject(function(_$controller_, _$timeout_, _webDevTec_, _toastr_) {
      spyOn(_webDevTec_, 'getTec').and.returnValue([{}, {}, {}, {}, {}]);
      spyOn(_toastr_, 'info').and.callThrough();

      vm = _$controller_('MainController');
      $timeout = _$timeout_;
      toastr = _toastr_;
    }));

    it('should have a timestamp creation date', function() {
      expect(vm.creationDate).toEqual(jasmine.any(Number));
    });

    it('should define animate class after delaying timeout ', function() {
      $timeout.flush();
      expect(vm.classAnimation).toEqual('rubberBand');
    });

    it('should show a Toastr info and stop animation when invoke showToastr()', function() {
      vm.showToastr();
      expect(toastr.info).toHaveBeenCalled();
      expect(vm.classAnimation).toEqual('');
    });

    it('should define more than 5 awesome things', function() {
      expect(angular.isArray(vm.awesomeThings)).toBeTruthy();
      expect(vm.awesomeThings.length === 5).toBeTruthy();
    });
  });
})();










/*
 * @package AngularJS
 * @version 2
 * @File Type: Javascript
 * @Author Gnanavel
 * @created date 8 Mar 2017
 * @Description  This is user controller file which contain functionalities for various actions related to all the users such as teacher,students
*/



app.controller('UserController', UserController);

  /** @ngInject */
  function UserController($timeout,$scope,$location,$http) {
    var vm = this;
	


	
	$scope.form = [];
	      $scope.files = [];

	      $scope.submit = function() {
	      	$scope.form.image = $scope.files[0];
			
			/* alert("Hello World"); */
			

	      	$http({
			  method  : 'POST',
			  url     : '/app/views/upload.php',
			  processData: false,
			  transformRequest: function (data) {
			      var formData = new FormData();
			      formData.append("image", $scope.form.image);  
			      return formData;  
			  },  
			  data : $scope.form,
			  headers: {
			         'Content-Type': undefined
			  }
		   }).success(function(data){
		        alert(data);
		   });

	      };

	      $scope.uploadedFile = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source = event.target.result
		      $scope.$apply(function($scope) {
		        $scope.files = element.files;
		      });
		    }
                    reader.readAsDataURL(element.files[0]);
		  }











		 //updating the chat conversation between teacher and students 	

		$scope.chat = function() {
		          $http({
		              
		              method: 'POST',
		              url: 'https://jitsi.aryvarttechnologies.com/chat.php',

				      params: {
		    			    msg : angular.element(".chat-msg-box").val()
					  }
		              
		          }).then(function (response) {
		              
		              // on success
		              $scope.chat_message = response.data;
		              angular.element(".chat-msg-box").val("");
		              
		          }, function (response) {
		              
		              // on error
		              console.log(response.data,response.status);
		              
		          });
		    }

		//Getting list of active students
		$http.get("https://jitsi.aryvarttechnologies.com/initial_chat.php")   
		.then(function (response) {$scope.chat_message = response.data;});
		
		//Getting list of active students
		$http.get("https://jitsi.aryvarttechnologies.com/get_students.php")   
		.then(function (response) {$scope.students = response.data;});
		
		//Getting list of students who shared the files
		$http.get("https://jitsi.aryvarttechnologies.com/file_share_students.php")   
		   .then(function (response) {$scope.file_shared_students = response.data;});

		//Getting list of chat students 
		$http.get("https://jitsi.aryvarttechnologies.com/chat_students.php")   
		   .then(function (response) {$scope.chat_students = response.data;});

		//Getting even details
		$http.get("https://jitsi.aryvarttechnologies.com/get_event_details.php")   
		.then(function (response) {$scope.event_details = response.data;});   

	 	//It call the screen2 page
    	$scope.callPage2 = function(){
    		$location.path("/screen2");
    	}

      	//It call the screen3 page
    	$scope.teacher = function(){
    		$location.path("/screen3");
    	}

      	//It call the upload2 page
    	$scope.upload = function(){
    		$location.path("/upload2");
    	}

      	//It call the home page
        $scope.home = function(){
        	$location.path("/main");
        }

      	//It call the screen2 page
      	$scope.screen2 = function(){
        	$location.path("/screen2");
      	}

      	//It call the screen3 page
      	$scope.screen3 = function(){
        	$location.path("/screen3");
      	}

      	


  }


