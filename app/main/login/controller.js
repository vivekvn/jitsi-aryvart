/* global app, FB */

app.controller('LoginCtrl', ['$scope', 'DataService', 'vcRecaptchaService', '$location', '$window', '$state', 'ConstantService',
    function ($scope, dataService, vcRecaptchaService, $location, $window, state, ConstantService) {

        // scroll top when enter page
        document.body.scrollTop = document.documentElement.scrollTop = 0;

        $scope.pulicKey = ConstantService.recaptcha_public_key;
        $scope.userError;
        $scope.passwordNotMatch;


        //Error Messages Declaration   
        $scope.recaptchaErrorMsg;
        $scope.incorrectUserErrorMsg;
        $scope.inactiveUserErrorMsg;
        $scope.incorrectPasswordErrorMsg;


        // Setting ReCaptcha ID
        $scope.setRecaptchaId = function (widgetId) {
            $scope.recaptchaId = widgetId;
        };

        //Getting language from php controller
        var postLanguageData = 'group_key=login_page';
        dataService.postData('/v1/lang/group/get', postLanguageData, localStorage.token).then(
            function (response) {

                var rstz = angular.fromJson(response.data);
                rstz = angular.fromJson(response.data); // for firefox only
                $scope.languageResult = rstz;

                $scope.recaptchaErrorMsg = $scope.languageResult.recapthca_error_msg;
                $scope.incorrectUserErrorMsg = $scope.languageResult.incorrect_user_error_msg;
                $scope.inactiveUserErrorMsg = $scope.languageResult.inactive_user_error_msg;
                $scope.incorrectPasswordErrorMsg = $scope.languageResult.incorrect_password_error_msg;


            });

        // Check reCaptcha Validate
        $scope.checkReCaptcha = function () {
            $scope.submitted = true;
            var ReCaptchaResponse = vcRecaptchaService.getResponse($scope.recaptchaId);

            if (ReCaptchaResponse === "") {
                $scope.reCaptchaErrorMsg = $scope.recaptchaErrorMsg;
            } else {
                $scope.reCaptchaErrorMsg = "";
            }

        };

        $scope.signInSuccess = function (res) {
            var result = res.data
            localStorage.token = result.userToken;
            localStorage.code = result.userCode;

            localStorage.signedIn = true;
            localStorage.isProfileComplete = result.isProfileComplete
            localStorage.isCatComplete = result.isCategoryComplete

            location.href = '/';
        };

        $scope.signInFail = function (res) {
            console.log('signin fail', res);
            $scope.showToast(res.data.message);
        };

        $scope.showToast = function (msg) {
            var $toastContent = $("<div style='line-height:inherit;'>\n\
                                <i class='material-icons' style='font-size:20px;'>error</i>\n\
                            <span>" + msg + "</span></div>");
            Materialize.toast($toastContent, 3000);
        };

        // Click Login 
        $scope.loginClick = function (isValid) {
            console.log('Valid', isValid);
            //if (isValid) {
            $scope.username;
            $scope.password;
            var ReCaptchaResponse = vcRecaptchaService.getResponse($scope.recaptchaId);

            if (ReCaptchaResponse === "") {
                //if string is empty
                if (ReCaptchaResponse === "") {
                    $scope.reCaptchaErrorMsg = $scope.recaptchaErrorMsg;
                } else {
                    $scope.reCaptchaErrorMsg = "";
                }

            } else {

                var form_data = {//prepare payload for request
                    'username': $scope.username,
                    'password': $scope.password,
                    'g-recaptcha-response': ReCaptchaResponse  //send g-captcah-reponse to our server
                };

                state.go('load', {
                    tasks: [
                        {
                            message: 'Signing in...',
                            url: '/v1/auth/login',
                            data: form_data,
                            task: dataService.post,
                            success: $scope.signInSuccess,
                            fail: $scope.signInFail
                        }
                    ]
                });

            }

            // }
        };


    }]);
