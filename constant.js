app.service('ConstantService', [function () {
    var host = 'http://int-fe.localdomain'

    this.api_prefix = '/v1'
    this.path_prefix = '/v1/'                                       // used by http-service

    this.recaptcha_public_key = '6LcrbSkTAAAAAKN9dG7zLSZntDsC65ipKGrPk_r6';     // Recaptcha Login/Register
    this.fb_app_id = '396240710717044';                                         // Facebook

    this.websocket_host = 'wss://ws.test.yourflairs.com/int-ws-api/'
}]);
