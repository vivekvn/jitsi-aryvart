app.directive('intLoading', [function () {
    return {
        restrict: 'EA',
        templateUrl: '/commons/loading/template.html',
        scope: {
            show: '=intLoading',
        },
        link: function (scope) {

        }
    }
}])