/**
 * INTELLIGENCE LTD ("COMPANY") CONFIDENTIAL Unpublished Copyright (c) 2016 INTELLIGENCE, All Rights
 * Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of COMPANY. The
 * intellectual and technical concepts contained herein are proprietary to COMPANY and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from COMPANY. Access to the source code
 * contained herein is hereby forbidden to anyone except current COMPANY employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering
 * such access.
 * 
 * The copyright notice above does not evidence any actual or intended publication or disclosure of
 * this source code, which includes information that is confidential and/or proprietary, and is a
 * trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR
 * PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF
 * COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.
 * THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY
 * ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */


app.factory('Dialog', [function () {
    var body = document.getElementsByTagName('body')[0]

    var dialog = function (data) {
        var self = this

        self.dismissable = data.dismissable
        self.title = data.title
        self.message = data.message
        self.buttons = data.buttons
        self.overlay = undefined
        self.dialog = undefined
        self.open = function () {
            reDraw($(self.overlay)).then(function () {
                self.overlay.style.opacity = '1'
                self.overlay.style.pointerEvents = 'auto'
                self.dialog.style.transform = 'translateX(-50%) scale(1,1)'
            })
        }
        self.promise = new Promise(function (resolve, reject) {
            self.close = function (resolved, answer) {
                reDraw($(self.overlay)).then(function () {
                    self.overlay.style.opacity = '0'
                    self.overlay.style.pointerEvents = 'none'
                    self.dialog.style.transform = 'translateX(-50%) scale(0,0)'
                    if (resolved)
                        resolve(answer)
                    else
                        reject(answer)
                    setTimeout(function () {
                        if (self.buttons[answer] && self.buttons[answer].onclick) self.buttons[answer].onclick()
                        $(self.overlay).remove()
                    }, 400)
                })
            }
        })

        self.init = function () {
            var overlay = document.createElement("div")
            var dialog = document.createElement("div")
            var content = document.createElement("div")
            var title = document.createElement("h4")
            var message = document.createElement("p")
            var buttons = document.createElement("div")
            overlay.className = 'valign-wrapper'
            overlay.style.position = 'fixed'
            overlay.style.zIndex = '1500' // materialize modal z-index is around 1000
            overlay.style.top = '0'
            overlay.style.left = '0'
            overlay.style.right = '0'
            overlay.style.bottom = '0'
            overlay.style.background = 'rgba(0,0,0,.4)'
            overlay.style.opacity = '0'
            overlay.style.pointerEvents = 'none'
            overlay.style.transition = 'opacity .4s'

            dialog.className = 'valign white card'
            content.className = 'card-content'
            title.className = 'center-align'
            message.className = 'left-align'
            buttons.className = 'card-action center-align'

            dialog.style.position = 'relative'
            dialog.style.minWidth = '350px'
            dialog.style.maxWidth = '100%'
            dialog.style.left = '50%'
            dialog.style.transform = 'translateX(-50%) scale(0,0)'
            dialog.style.transition = 'transform .4s'
            message.style.minHeight = '80px'

            title.innerText = self.title
            // message.innerText = self.message
            message.innerHTML = self.message

            //                if (self.dismissable)
            //                    overlay.onclick = function () {
            //                        self.close(false, 'reject')
            //                    }
            //                dialog.onclick = function (e) {
            //                    e.stopPropagation()
            //                }

            if (self.buttons.length > 0) {
                for (var i = 0; i < self.buttons.length; i++) {
                    var button = document.createElement('a')
                    button.className = 'btn-flat'
                    button.innerText = self.buttons[i].label
                    button.setAttribute('button-id', i)
                    button.onclick = function () {
                        self.close(true, this.getAttribute('button-id'))
                    }
                    buttons.appendChild(button)
                }
            } else {
                var button = document.createElement('a')
                button.className = 'btn-flat'
                button.innerText = 'OK'
                button.onclick = function () {
                    self.close(true, 'ok')
                }
                buttons.appendChild(button)
            }

            content.appendChild(title)
            content.appendChild(message)
            dialog.appendChild(content)
            dialog.appendChild(buttons)
            overlay.appendChild(dialog)

            self.overlay = overlay
            self.dialog = dialog
            body.appendChild(overlay)
            self.open()
        }
        self.init()
        return self.promise
    }

    return dialog
}])

app.factory('LoadingDialog', [function () {
    var body = document.getElementsByTagName('body')[0]

    var loadingDialog = function (data) {
        var self = this

        self.tasks = data
        self.overlay = undefined
        self.dialog = undefined
        self.message = undefined
        self.open = function () {
            reDraw($(self.overlay)).then(function () {
                self.overlay.style.opacity = '1'
                self.overlay.style.pointerEvents = 'auto'
                self.dialog.style.transform = 'translateX(-50%) scale(1,1)'
                setTimeout(function () {
                    self.execute()
                }, 400)
            })
        }
        self.promise = new Promise(function (resolve, reject) {
            self.close = function (resolved, answer) {
                reDraw($(self.overlay)).then(function () {
                    self.overlay.style.opacity = '0'
                    self.overlay.style.pointerEvents = 'none'
                    self.dialog.style.transform = 'translateX(-50%) scale(0,0)'
                    if (resolved)
                        resolve(answer)
                    else
                        reject(answer)
                    setTimeout(function () {
                        $(self.overlay).remove()
                    }, 400)
                })
            }
        })
        self.counter = 0
        self.execute = function () {
            self.message.innerText = self.tasks[self.counter].message.toUpperCase()
            self.tasks[self.counter].task(self.tasks[self.counter].url, self.tasks[self.counter].data).then(self.success, self.fail)
        }
        self.success = function (res) {
            try {
                var data = angular.fromJson(res.data)
                data = angular.fromJson(data)
                console.log(res.status, res.config.url, data)
            } catch (e) {
                console.log(res.status, res.config.url, res.data)
            }
            self.tasks[self.counter].success(res)
            if (self.counter < self.tasks.length - 1) {
                self.counter++
                self.execute()
            } else {
                self.close(true, res)
            }
        }
        self.fail = function (res) {
            try {
                console.log(res.status, res.config.url, angular.fromJson(res.data))
            } catch (e) {
                console.log(res.status, res.config.url, res.data)
            }
            self.tasks[self.counter].fail(res)
            self.close(false, res)
        }

        self.init = function () {
            var overlay = document.createElement("div")
            var dialog = document.createElement("div")
            var content = document.createElement("div")
            var spinner = document.createElement("div")
            var message = document.createElement("h3")
            overlay.className = 'valign-wrapper'
            overlay.style.position = 'fixed'
            overlay.style.zIndex = '2000' // materialize modal z-index is around 1000
            overlay.style.top = '0'
            overlay.style.left = '0'
            overlay.style.right = '0'
            overlay.style.bottom = '0'
            overlay.style.background = 'rgba(0,0,0,.4)'
            overlay.style.opacity = '0'
            overlay.style.pointerEvents = 'none'
            overlay.style.transition = 'opacity .4s'

            dialog.className = 'valign'
            content.className = 'card-content center-align'
            message.className = 'white-text'

            dialog.style.position = 'relative'
            dialog.style.minWidth = '350px'
            dialog.style.maxWidth = '100%'
            dialog.style.left = '50%'
            dialog.style.transform = 'translateX(-50%) scale(0,0)'
            dialog.style.transition = 'transform .4s'
            message.style.minHeight = '80px'

            // spinner.src = '/commons/dialog/preloader.gif'
            $(spinner).load( "commons/loading/template.html" )

            content.appendChild(spinner)
            content.appendChild(message)
            dialog.appendChild(content)
            overlay.appendChild(dialog)

            self.overlay = overlay
            self.dialog = dialog
            self.message = message
            body.appendChild(overlay)
            self.open()
        }
        self.init()
        return self.promise
    }

    return loadingDialog
}])

app.service('DialogService', ['Dialog', 'LoadingDialog', function (Dialog, LoadingDialog) {
    this.show = function (data) {
        return new Dialog(data)
    }

    this.load = function (data) {
        return new LoadingDialog(data)
    }
}])