app.factory('IntModalFactory', [function () {
    return {
        data: undefined,
        setData: function (newData) { this.data = newData },
        getData: function () { return this.data }
    }
}])

app.controller('ModalCtrl', ['$scope', 'IntModalFactory', function (scope, factory) {
    var self = this
    this.scope = scope

    self.setData = function (newDat) { scope.$$childTail.setData(newDat) }
    self.setFunction = function (fn) { scope.$$childTail.setFunction(fn) }

}])

app.directive('intModal', ['$compile', 'IntModalFactory', function (compile, factory) {
    var modalCtrl = {}
    var body = $('body')
    return {
        restrict: 'A',
        scope: {
            modal: '@intModal',
            intController: '@',
            id: '@',
            backdrop: '=?',
            data: '=?',
            form: '=?',
            callback: '=?'
        },
        controller: 'ModalCtrl',
        link: function (scope, element, attributes, ctrl) {
            if (!modalCtrl[scope.intController]) modalCtrl[scope.intController] = ctrl
            scope.modalElem = $('#int-' + scope.id)
            if (scope.modalElem.length == 0) {
                var modal = $('<div class="valign int-modal" ng-controller="' + scope.intController + ' as ctrl" ng-include="\'' + scope.modal + '\'"></div>')
                scope.modalElem = $('<div id="int-' + scope.id + '" class="valign-wrapper int-overlay"></div>')
                if (scope.backdrop) scope.modalElem.addClass("int-backdrop")
                scope.modalElem.append(modal)
                body.append(scope.modalElem)
                compile(scope.modalElem)(scope)
            }

            element.click(function () {
                var data = []
                var fn = []
                data[0] = { key: 'modalData', val: scope.data }
                if (scope.form) data.push({ key: 'form', val: scope.form })
                if (scope.callback) fn.push({ key: 'callback', val: scope.callback })
                modalCtrl[scope.intController].setData(data)
                modalCtrl[scope.intController].setFunction(fn)
                reDraw(scope.modalElem).then(function () {
                    body.addClass('show-int-modal')
                    $(scope.modalElem).addClass('show')
                    $(scope.modalElem).scrollTop(0)
                })
            })

            scope.close = function () {
                $(scope.modalElem).removeClass('show')
                body.removeClass('show-int-modal')
            }
        }
    }
}])