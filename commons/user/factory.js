app.factory('User', [function () {

    var User = function (user) {
        var self = this
        self.changePwdRequired = user.changePwdRequired
        self.isCategoryComplete = user.isCategoryComplete
        self.isProfileComplete = user.isProfileComplete
        self.userPictureCode = user.userPictureCode
        self.userType = user.userType
        self.username = user.username
        self.onVacationMode = user.onVacationMode
        self.set = function (key, val) { self[key] = val }
    }

    return User
}])