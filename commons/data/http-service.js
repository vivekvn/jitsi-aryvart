app.service('HTTPService', ['$http', 'ConstantService', '$q', '$rootScope',
    function (http, constant, $q, rootScope) {

        var cancelableList = []
        this.cancel = function () {
            for (var i = 0; i < cancelableList.length; i++) {
                cancelableList[i].resolve()
            }
            cancelableList.length = 0
        }

        // Set Headers
        delete http.defaults.headers.common['X-Requested-With'];
        http.defaults.headers.common['int-fe-shadow'] = localStorage.token;
        //http.defaults.cache = true

        //------------------------------------- Default Callback -------------------------------------
        this.success = function (res) {
            console.log(res.config.url, res.data)
            return res.data
        }
        this.fail = function (res) {
            // if (res.status == 401 || res.status == 403) {
            if (res.status == 401) {
                delete localStorage.token
                delete localStorage.code
                delete localStorage.signedIn
                location.href = '/login';
            }
            else {
                console.log(res.config.url, 'fail', res)
                return undefined
            }
        }

        //------------------------------------- Get -------------------------------------
        this.get = function (url, cache) {
            var canceller = $q.defer()
            cancelableList.push(canceller)
            return http({
                cache: cache || false,
                method: 'GET',
                url: constant.path_prefix + url,
                timeout: canceller.promise,
                headers: { 'int-fe-shadow': localStorage.token }
            }).then(this.success, this.fail);
        };

        //------------------------------------- Put -------------------------------------
        this.put = function (url, params) {
            var canceller = $q.defer()
            cancelableList.push(canceller)
            var data = 'data=' + JSON.stringify(params)
            return http({
                method: 'PUT',
                url: constant.path_prefix + url,
                data: data,
                timeout: canceller.promise,
                headers: { 'int-fe-shadow': localStorage.token }
            }).then(this.success, this.fail);
        };

        //------------------------------------- Post -------------------------------------
        this.post = function (url, params, cache) {
            var canceller = $q.defer()
            cancelableList.push(canceller)
            var data = undefined
            if (params && params.translate)
                data = 'group_key=' + params.translate;
            else
                data = 'data=' + JSON.stringify(params)
            return http({
                method: 'POST',
                cache: cache || false,
                url: constant.path_prefix + url,
                data: data,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded', 'int-fe-shadow': localStorage.token },
                timeout: canceller.promise
            }).then(this.success, this.fail)
        };

        //------------------------------------- Delete -------------------------------------
        this.delete = function (url, params) {
            var canceller = $q.defer()
            cancelableList.push(canceller)
            var data = 'data=' + JSON.stringify(params)
            return http({
                method: 'DELETE',
                url: constant.path_prefix + url,
                data: data,
                timeout: canceller.promise,
                headers: { 'int-fe-shadow': localStorage.token }
            }).then(this.success, this.fail)
        };

    }]);
