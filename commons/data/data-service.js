/* global app */

app.service('DataService', function ($http, $window, $location, $state, $rootScope, $q) {

    delete $http.defaults.headers.common['X-Requested-With'];
    var cancelableList = []

    this.cancel = function () {
        for (var i = 0; i < cancelableList.length; i++) {
            cancelableList[i].resolve()
        }
        cancelableList.length = 0
    }

    this.get = function (url, cache) {
        var canceller = $q.defer()
        cancelableList.push(canceller)
        return $http({
            cache: cache || false,
            method: 'GET',
            url: url,
            headers: { 'int-fe-shadow': localStorage.token },
            timeout: canceller.promise
        }).success(function (response) {

        }).error(function (response, status) {
            if (status === 401) {
                delete localStorage.token;
                delete localStorage.code;
                delete localStorage.isCatComplete;
                delete localStorage.isProfileComplete;
                delete localStorage.signedIn;

                FB.getLoginStatus(function (response) {
                    if (response.status === 'connected') {
                        FB.logout(function (response) {
                            // user is now logged out
                            console.log('FB user log out.');
                        });
                    }
                });
                location.href='/login';
            }
        });
    };
    this.put = function (url, obj) {
        var canceller = $q.defer();
        cancelableList.push(canceller)
        var data = 'data=' + JSON.stringify(obj)
        return $http({
            method: 'PUT',
            url: url,
            data: data,
            headers: { 'int-fe-shadow': localStorage.token },
            timeout: canceller.promise
        }).success(function (response) {

        }).error(function (response, status) {
            if (status === 401) {
                delete localStorage.token;
                delete localStorage.code;
                delete localStorage.isCatComplete;
                delete localStorage.isProfileComplete;
                delete localStorage.signedIn;

                FB.getLoginStatus(function (response) {
                    if (response.status === 'connected') {
                        FB.logout(function (response) {
                            // user is now logged out
                            console.log('FB user log out.');
                        });
                    }
                });
                 location.href='/login';
            }
        });
    };
    this.post = function (url, obj, cache) {
        var canceller = $q.defer();
        cancelableList.push(canceller)
        var data = undefined
        if (obj && obj.translate)
            data = 'group_key=' + obj.translate;
        else
            data = 'data=' + JSON.stringify(obj)
        return $http({
            method: 'POST',
            cache: cache || false,
            url: url,
            data: data,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'int-fe-shadow': localStorage.token
            },
            timeout: canceller.promise
        }).success(function (response) {
            
        }).error(function(response , status){			        
            if (status === 401) {       
                delete localStorage.token;
                delete localStorage.code;
                delete localStorage.isCatComplete;
                delete localStorage.isProfileComplete;
                delete localStorage.signedIn;

                FB.getLoginStatus(function (response) {
                    if (response.status === 'connected') {
                        FB.logout(function (response) {
                            // user is now logged out
                            console.log('FB user log out.');
                        });
                    }
                });
                location.href='/login';
            }
            return response
        });
    };

    this.delete = function (url, obj) {
        var canceller = $q.defer();
        cancelableList.push(canceller)
        var data = 'data=' + JSON.stringify(obj)
        return $http({
            method: 'DELETE',
            url: url,
            data: data,
            headers: { 'int-fe-shadow': localStorage.token },
            timeout: canceller.promise
        }).success(function (response) {
            
        }).error(function(response,status){			        
            if (status === 401) {         
                delete localStorage.token;
                delete localStorage.code;
                delete localStorage.isCatComplete;
                delete localStorage.isProfileComplete;
                delete localStorage.signedIn;

                FB.getLoginStatus(function (response) {
                    if (response.status === 'connected') {
                        FB.logout(function (response) {
                            // user is now logged out
                            console.log('FB user log out.');
                        });
                    }
                });
                 location.href='/login';
            }
        });
    };

    this.getData = function (url, token) {

        return $http({
            method: 'GET',
            url: url,
            headers: { 'int-fe-shadow': token, 'Content-Type': undefined }
        }).success(function (response) {
            
        }).error(function(response,status){			        
            if (status === 401) {           
                delete localStorage.token;
                delete localStorage.code;
                delete localStorage.isCatComplete;
                delete localStorage.isProfileComplete;
                delete localStorage.signedIn;

                FB.getLoginStatus(function (response) {
                    if (response.status === 'connected') {
                        FB.logout(function (response) {
                            // user is now logged out
                            console.log('FB user log out.');
                        });
                    }
                });
                 location.href='/login';
            }
        });

    };

    this.deleteData = function (url, token) {

        return $http({
            method: 'DELETE',
            url: url,
            headers: { 'int-fe-shadow': token, 'Content-Type': undefined }
        }).success(function (response) {

        }).error(function (response, status) {
            if (status === 401) {
                delete localStorage.token;
                delete localStorage.code;
                delete localStorage.isCatComplete;
                delete localStorage.isProfileComplete;
                delete localStorage.signedIn;

                FB.getLoginStatus(function (response) {
                    if (response.status === 'connected') {
                        FB.logout(function (response) {
                            // user is now logged out
                            console.log('FB user log out.');
                        });
                    }
                });
                 location.href='/login';
            }
        });

    };

    this.getDataAuth = function (url, token) {
        return $http({
            method: 'GET',
            url: url,
            headers: { 'int-fe-shadow': token }
        }).success(function () {

        }).error(function(response,status){			        
            if (status === 401) {       
                delete localStorage.token;
                delete localStorage.code;
                delete localStorage.isCatComplete;
                delete localStorage.isProfileComplete;
                delete localStorage.signedIn;

                FB.getLoginStatus(function (response) {
                    if (response.status === 'connected') {
                        FB.logout(function (response) {
                            // user is now logged out
                            console.log('FB user log out.');
                        });
                    }
                });
                 location.href='/login';
            }
        });
    };

    this.putData = function (url, data) {
        return $http.put(url, data);
    };

    this.postData = function (url, postdata, token) {

        return $http({
            method: 'POST',
            url: url,
            data: postdata,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded', 'int-fe-shadow': token }
        }).success(function () {

        }).error(function(response,status){			        
            if (status === 401) {         
                delete localStorage.token;
                delete localStorage.code;
                delete localStorage.isCatComplete;
                delete localStorage.isProfileComplete;
                delete localStorage.signedIn;

                FB.getLoginStatus(function (response) {
                    if (response.status === 'connected') {
                        FB.logout(function (response) {
                            // user is now logged out
                            console.log('FB user log out.');
                        });
                    }
                });
                location.href='/login';
            }
        });

    };

    this.postFormData = function (url, postdata, token) {

        return $http({
            method: 'POST',
            url: url,
            data: postdata,
            headers: { 'Content-Type': undefined, 'int-fe-shadow': token }
        }).success(function () {

        }).error(function (response, status) {
            if (status === 401) {
                delete localStorage.token;
                delete localStorage.code;
                delete localStorage.isCatComplete;
                delete localStorage.isProfileComplete;
                delete localStorage.signedIn;

                FB.getLoginStatus(function (response) {
                    if (response.status === 'connected') {
                        FB.logout(function (response) {
                            // user is now logged out
                            console.log('FB user log out.');
                        });
                    }
                });
                location.href='/login';
            }
        });

    };

    this.postJSONData = function (url, postdata, token) {

        return $http({
            method: 'POST',
            url: url,
            data: postdata,
            headers: { 'Content-Type': 'application/json', 'int-fe-shadow': token }
        }).success(function () {

        }).error(function(response,status){			        
            if (status === 401) {             
                
                delete localStorage.token;
                delete localStorage.code;
                delete localStorage.isCatComplete;
                delete localStorage.isProfileComplete;
                delete localStorage.signedIn;

                FB.getLoginStatus(function (response) {
                    if (response.status === 'connected') {
                        FB.logout(function (response) {
                            // user is now logged out
                            console.log('FB user log out.');
                        });
                    }
                });
               
                location.href='/login';
            }
        });

    };

});
