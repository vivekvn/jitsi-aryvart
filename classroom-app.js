/* global $, JitsiMeetJS */

const options = {
    hosts: {
        domain: 'jitsi.aryvarttechnologies.com',
        muc: 'conference.jitsi.aryvarttechnologies.com', // FIXME: use XEP-0030
        bridge: 'jitsi-videobridge.jitsi.aryvarttechnologies.com'

    },
    bosh: '//jitsi.aryvarttechnologies.com/http-bind', // FIXME: use xep-0156 for that

    // The name of client node advertised in XEP-0115 'c' stanza
    clientNode: 'http://jitsi.org/jitsimeet'
};

const confOptions = {
    openSctp: true
};

let connection = null;
let isJoined = false;
let room = null;

let localTracks = [];
const remoteTracks = {};


function getConferenceName() {
    return localStorage.getItem("classroom-name");
}

function isStudent() {
    if (localStorage.getItem("classroom-login") === "student")
        return true;
    else return false;
}

function isTeacher() {
    if (localStorage.getItem("classroom-login") === "teacher")
        return true;
    else return false;
}
/**
 * Handles local tracks.
 * @param tracks Array with JitsiTrack objects
 */
function onLocalTracks(tracks) {
    localTracks = tracks;
    for (let i = 0; i < localTracks.length; i++) {
        localTracks[i].addEventListener(
            JitsiMeetJS.events.track.TRACK_AUDIO_LEVEL_CHANGED,
            audioLevel => console.log(`Audio Level local: ${audioLevel}`));
        localTracks[i].addEventListener(
            JitsiMeetJS.events.track.TRACK_MUTE_CHANGED,
            () => console.log('local track muted'));
        localTracks[i].addEventListener(
            JitsiMeetJS.events.track.LOCAL_TRACK_STOPPED,
            () => console.log('local track stoped'));
        localTracks[i].addEventListener(
            JitsiMeetJS.events.track.TRACK_AUDIO_OUTPUT_CHANGED,
            deviceId =>
                console.log(
                    `track audio output device was changed to ${deviceId}`));
        if (localTracks[i].getType() === 'video') {
            if (isStudent()) {
                
                $('.video-container').append(`<video autoplay='1' class='video-invisible' id='localVideo${i}' />`);
            }
            else 
            {
                $('.video-container').append(`<video autoplay='1' class='video-invisible' id='localVideo${i}' />`);
            }
            localTracks[i].attach($(`#localVideo${i}`)[0]);
        } else {
            if (isStudent()) break;
            $('.video-container').append(
                `<audio autoplay='1' muted='true' id='localAudio${i}' />`);
            localTracks[i].attach($(`#localAudio${i}`)[0]);
        }
        
    }
}

var isPublishing= false;

function publishTracks() {
    
    for (let i = 0; i < 2; i++) {
        if(isPublishing)
            localTracks[i].unmute()
        else
            room.addTrack(localTracks[i]);
    }
    isPublishing = true;
    $('.start-publish').css({"color":"grey"});
    $('.stop-publish').css({"color":"green"});
}

function unpublishTracks() {
    if(!isPublishing)
        return;
    for (let i = 0; i < 2; i++) {
        //room.removeTrack(localTracks[i]);
        localTracks[i].mute();
        if (localTracks[i].getType() === 'video')
            localTracks[i].attach($(`#localVideo${i}`)[0]);
    }
    //isPublishing = false;
    $('.stop-publish').css({"color":"grey"});
    setTimeout(function(){ $('.start-publish').css({"color":"green"}); }, 10000);
}

var numnberOfVideosAdded = 0, numberOfAudiosAdded = 0;
/**
 * Handles remote tracks
 * @param track JitsiTrack object
 */
function onRemoteTrack(track) {
    if (track.isLocal() || isTeacher()) {
        return;
    }
    
    console.log("GOT REMOTE TRACK");
    const participant = track.getParticipantId  ();

    if (!remoteTracks[participant]) {
        remoteTracks[participant] = [];
    }
    const idx = remoteTracks[participant].push(track);

    track.addEventListener(
        JitsiMeetJS.events.track.TRACK_AUDIO_LEVEL_CHANGED,
        audioLevel => console.log(`Audio Level remote: ${audioLevel}`));
    track.addEventListener(
        JitsiMeetJS.events.track.TRACK_MUTE_CHANGED,
        () => console.log('remote track muted'));
    track.addEventListener(
        JitsiMeetJS.events.track.LOCAL_TRACK_STOPPED,
        () => console.log('remote track stoped'));
    track.addEventListener(JitsiMeetJS.events.track.TRACK_AUDIO_OUTPUT_CHANGED,
        deviceId =>
            console.log(
                `track audio output device was changed to ${deviceId}`));
    const id = participant + track.getType() + idx;

    if (track.getType() === 'video') {
        if(isStudent() && $('.video-container').find('.video-local').html() ===  "")
            return;
        if ($('.video-container').find('.video-local').html() ===  "")
        {
            $('.video-container').find('.video-local').addClass("video-local1");
            $('.video-container').find('.video-local1').removeClass("video-local");
            $('.video-container').prepend(
            `<video class="video-local2" autoplay='1' id='${participant}video${idx}' />`);
        }
        else
        {
            if(removedVideo) location.reload();
            $('.video-container').append(
            `<video class="video-local" autoplay='1' id='${participant}video${idx}' />`);
            numnberOfVideosAdded = 1;
        }
    } else {
        if($('.video-container').find('audio').html() === "")
            return;
        $('.video-container').append(
            `<audio autoplay='1' id='${participant}audio${idx}' />`);
            numberOfAudiosAdded = 1;
    }
    track.attach($(`#${id}`)[0]);
}

/**
 * That function is executed when the conference is joined
 */
function onConferenceJoined() {
    console.log('conference joined!');
    isJoined = true;
    //for (let i = 0; i < localTracks.length; i++) {
    //    room.addTrack(localTracks[i]);
    //}
}

/**
 *
 * @param id
 */
function onUserLeft(id) {
    console.log('user left');
    if (!remoteTracks[id]) {
        return;
    }
    const tracks = remoteTracks[id];

    for (let i = 0; i < tracks.length; i++) {
        tracks[i].detach($(`#${id}${tracks[i].getType()}`));
    }
}

var removedVideo = false;
/**
 * That function is called when connection is established successfully
 */
function onConnectionSuccess() {
    room = connection.initJitsiConference(getConferenceName(), confOptions);
    room.on(JitsiMeetJS.events.conference.TRACK_ADDED, onRemoteTrack);
    room.on(JitsiMeetJS.events.conference.TRACK_REMOVED, track => {
        if(isTeacher())
            return;

        $("video").remove();
        $("audio").remove();
        removedVideo = true;
        
        console.log(`track removed!!!${track}`);
    });
    room.on(
        JitsiMeetJS.events.conference.CONFERENCE_JOINED,
        onConferenceJoined);
    room.on(JitsiMeetJS.events.conference.USER_JOINED, id => {
        console.log('user join');
        remoteTracks[id] = [];
    });
    room.on(JitsiMeetJS.events.conference.USER_LEFT, onUserLeft);
    room.on(JitsiMeetJS.events.conference.TRACK_MUTE_CHANGED, track => {
        if(isTeacher())
            return;
        if(track.getType() === 'video')
            $(".video-local").toggle();
        console.log(`${track.getType()} - ${track.isMuted()}`);
    });
    room.on(
        JitsiMeetJS.events.conference.DISPLAY_NAME_CHANGED,
        (userID, displayName) => console.log(`${userID} - ${displayName}`));
    room.on(JitsiMeetJS.events.conference.TRACK_AUDIO_LEVEL_CHANGED,
      (userID, audioLevel) => {
          console.log(`${userID} - ${audioLevel}`);
      });
    room.on(JitsiMeetJS.events.conference.RECORDER_STATE_CHANGED, () => {
        console.log(`${room.isRecordingSupported()} - ${
             room.getRecordingState()} - ${
             room.getRecordingURL()}`);
    });
    room.on(JitsiMeetJS.events.conference.PHONE_NUMBER_CHANGED, () => {
        console.log(
            `${room.getPhoneNumber()} - ${
             room.getPhonePin()}`);
    });
    room.join();
}

/**
 * This function is called when the connection fail.
 */
function onConnectionFailed() {
    console.error('Connection Failed!');
}

/**
 * This function is called when the connection fail.
 */
function onDeviceListChanged(devices) {
    console.info('current devices', devices);
}

/**
 * This function is called when we disconnect.
 */
function disconnect() {
    console.log('disconnect!');
    connection.removeEventListener(
        JitsiMeetJS.events.connection.CONNECTION_ESTABLISHED,
        onConnectionSuccess);
    connection.removeEventListener(
        JitsiMeetJS.events.connection.CONNECTION_FAILED,
        onConnectionFailed);
    connection.removeEventListener(
        JitsiMeetJS.events.connection.CONNECTION_DISCONNECTED,
        disconnect);
}

/**
 *
 */
function unload() {
    for (let i = 0; i < localTracks.length; i++) {
        localTracks[i].stop();
    }
    room.leave();
    connection.disconnect();
}

let isVideo = true;

/**
 *
 */
function switchVideo() { // eslint-disable-line no-unused-vars
    isVideo = !isVideo;
    if (localTracks[1]) {
        localTracks[1].dispose();
        localTracks.pop();
    }
    if (isVideo) {
        JitsiMeetJS.createLocalTracks(
                { devices: isVideo ? [ 'video' ] : [ 'desktop' ] })
            .then(tracks => {
                localTracks.push(tracks[0]);
                localTracks[1].addEventListener(
                    JitsiMeetJS.events.track.TRACK_MUTE_CHANGED,
                    () => console.log('local track muted'));
                localTracks[1].addEventListener(
                    JitsiMeetJS.events.track.LOCAL_TRACK_STOPPED,
                    () => console.log('local track stoped'));
                localTracks[1].attach($('#localVideo1')[0]);
                //room.addTrack(localTracks[1]);
            })
            .catch(error => console.log(error));
    }
}

/**
 *
 * @param selected
 */
function changeAudioOutput(selected) { // eslint-disable-line no-unused-vars
    JitsiMeetJS.mediaDevices.setAudioOutputDevice(selected.value);
}

window.onload = function(e) {
    $(window).bind('beforeunload', unload);
    $(window).bind('unload', unload);


if(isTeacher()) {
        $('.video-container').append(`<video autoplay='1' class='video-local' id='localCam' />`);

        var video = document.querySelector("#localCam");
        
        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;
        
        if (navigator.getUserMedia) {       
            navigator.getUserMedia({video: true}, handleVideo, videoError);
        }
        
        function handleVideo(stream) {
            video.src = window.URL.createObjectURL(stream);
        }
        
        function videoError(e) {
            // do something
        }
}
    // JitsiMeetJS.setLogLevel(JitsiMeetJS.logLevels.ERROR);
    const initOptions = {
        disableAudioLevels: true,

        // The ID of the jidesha extension for Chrome.
        desktopSharingChromeExtId: 'injejffpbileadfdjhjghdhilcibocic',

        // Whether desktop sharing should be disabled on Chrome.
        desktopSharingChromeDisabled: false,

        // The media sources to use when using screen sharing with the Chrome
        // extension.
        desktopSharingChromeSources: [ 'screen', 'window' ],

        // Required version of Chrome extension
        desktopSharingChromeMinExtVersion: '0.1',

        // The ID of the jidesha extension for Firefox. If null, we assume that no
        // extension is required.
        desktopSharingFirefoxExtId: "jidesha@aryvarttechnologies.com",

        // Whether desktop sharing should be disabled on Firefox.
        desktopSharingFirefoxDisabled: false,

        // The maximum version of Firefox which requires a jidesha extension.
        // Example: if set to 41, we will require the extension for Firefox versions
        // up to and including 41. On Firefox 42 and higher, we will run without the
        // extension.
        // If set to -1, an extension will be required for all versions of Firefox.
        desktopSharingFirefoxMaxVersionExtRequired: -1,

        // The URL to the Firefox extension for desktop sharing.
        desktopSharingFirefoxExtensionURL: null
    };
    

    if ( localStorage.getItem("classroom-login") === undefined || localStorage.getItem("classroom-login") === null || localStorage.getItem("classroom-name") === null) 
        window.location.href = "login.html";

        
    JitsiMeetJS.init(initOptions)
        .then(() => {
            connection = new JitsiMeetJS.JitsiConnection(null, null, options);

            connection.addEventListener(
                JitsiMeetJS.events.connection.CONNECTION_ESTABLISHED,
                onConnectionSuccess);
            connection.addEventListener(
                JitsiMeetJS.events.connection.CONNECTION_FAILED,
                onConnectionFailed);
            connection.addEventListener(
                JitsiMeetJS.events.connection.CONNECTION_DISCONNECTED,
                disconnect);

            JitsiMeetJS.mediaDevices.addEventListener(
                JitsiMeetJS.events.mediaDevices.DEVICE_LIST_CHANGED,
                onDeviceListChanged);

            connection.connect();
            
        if (isTeacher())
            JitsiMeetJS.createLocalTracks({ devices: [ 'audio', 'video', 'video' ] })
                 .then(onLocalTracks)
                 .catch(error => {
                     throw error;
                 });
        })
        .catch(error => console.log(error));
	    /*if(window.location.href.indexOf("teacherdesktop")>0)
	        JitsiMeetJS.createLocalTracks({ devices: [ 'audio', 'desktop' ] })
                 .then(onLocalTracks)
                 .catch(error => {
                     throw error;
                 });
        else if (window.location.href.indexOf("studentdesktop")>0)
            JitsiMeetJS.createLocalTracks({ devices: [ 'audio','desktop' ] })
                 .then(onLocalTracks)
                 .catch(error => {
                     throw error;
                 });
	    else if (isStudent())
            return;
        else
             JitsiMeetJS.createLocalTracks({ devices: [ 'audio', 'video' ] })
                .then(onLocalTracks)
                .catch(error => {
                    throw error;
                });
        })*/
        

    if (JitsiMeetJS.mediaDevices.isDeviceChangeAvailable('output')) {
        JitsiMeetJS.mediaDevices.enumerateDevices(devices => {
            const audioOutputDevices
                = devices.filter(d => d.kind === 'audiooutput');

            if (audioOutputDevices.length > 1) {
                $('#audioOutputSelect').html(
                    audioOutputDevices
                        .map(
                            d =>
                                `<option value="${d.deviceId}">${d.label}</option>`)
                        .join('\n'));

                $('#audioOutputSelectWrapper').show();
            }
        });
    }
}
