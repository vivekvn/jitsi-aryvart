/*
 * @package AngularJS
 * @version 2
 * @File Type: Javascript
 * @Author Gnanavel
 * @created date 8 Mar 2017
 * @Description  This is route js file which conatin navigation path for all the template files 
*/



app.config([
    '$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/');
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/main/whiteboard/views/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
	  .state('screen4', {
        url: '/screen4',
        templateUrl: 'app/main/whiteboard/views/screen4.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
	  .state('screen5', {
        url: '/screen5',
        templateUrl: 'app/main/whiteboard/views/screen5.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
	  .state('screen2', {
        url: '/screen2',
        templateUrl: 'app/main/whiteboard/views/screen2.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
	   .state('screen3', {
        url: '/screen3',
        templateUrl: 'app/main/whiteboard/views/screen3.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
	  .state('screen6', {
        url: '/screen6',
        templateUrl: 'app/main/whiteboard/views/screen6.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
    
	  .state('upload', {
        url: '/upload',
        templateUrl: 'app/main/whiteboard/views/upload.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
	  
	   .state('upload2', {
        url: '/upload2',
        templateUrl: 'app/main/whiteboard/views/upload2.html',
        controller: 'MainController',
        controllerAs: 'main'
      });
  }]);


