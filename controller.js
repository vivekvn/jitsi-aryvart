/* global app */

/**
 * INTELLIGENCE LTD ("COMPANY") CONFIDENTIAL Unpublished Copyright (c) 2016 INTELLIGENCE, All Rights
 * Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of COMPANY. The
 * intellectual and technical concepts contained herein are proprietary to COMPANY and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly
 * forbidden unless prior written permission is obtained from COMPANY. Access to the source code
 * contained herein is hereby forbidden to anyone except current COMPANY employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering
 * such access.
 * 
 * The copyright notice above does not evidence any actual or intended publication or disclosure of
 * this source code, which includes information that is confidential and/or proprietary, and is a
 * trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR
 * PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF
 * COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.
 * THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY
 * ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
 * ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 */

app.controller('AppCtrl', ['$scope', 'DataService', '$state', '$window', 'HTTPService', 'preload', 'user', 'notificationTranslate', 'amMoment', '$rootScope',
    function (scope, dataService, state, window, http, preload, user, notificationTranslate, amMoment, rootScope) {
        var self = this;
        this.scope = scope;
        scope.translate = preload.translate
        self.translate = preload.translate
        self.isSignedIn = (localStorage.code) ? true : false;
        
        self.token = localStorage.token;
        self.ucode = localStorage.code;
        self.user = user;

        self.currentLanguage = preload.language.current;
        self.languages = preload.language.list;
        self.currencies = preload.currency.list;
        self.timezones = preload.timezone.list
        self.currentCurrency = preload.currency.current;
        self.currentTimezone = preload.timezone.current;
        
        if (self.currentTimezone) setTimezone(self.currentTimezone.name)
        else delete localStorage.timezone
       
        console.log('token', localStorage.token);
        console.log('user code', localStorage.code);

        //     Add scrollspy
        $('#top').scrollSpy({ scrollOffset: 0 })

        self.menu = {}

        self.menu['left'] = [
            { label: 'Home', icon: 'home', sref: 'home', class: '' },
            { label: 'Event', icon: 'business_center', sref: 'event', class: '' },
            { label: 'Whiteboard', sref: 'whiteboard', class: '' },
        ];

        self.confirmLogout = function () {
            state.go('dialog', {
                title: scope.translate.dialogue_logout,
                message: scope.translate.dialogue_logout_msg,
                buttons: [
                    { label: scope.translate.cancel_button },
                    { label: scope.translate.ok_button, onclick: self.logout }
                ]
            });
        };

        self.logout = function () {
            state.go('load', {
                tasks: [{
                    message: scope.translate.dialogue + '...',
                    url: '/v1/auth/logout',
                    task: dataService.post,
                    success: function (res) {
                        console.log("logout", res);
                        delete localStorage.token;
                        delete localStorage.code;
                        delete localStorage.isCatComplete;
                        delete localStorage.isProfileComplete;
                        delete localStorage.signedIn;
                       
                        location.href = '/';
                    },
                    fail: function (res) {
                        console.log('fail', res);
                    }
                }]
            });
        };

        self.click = function (selector) {
            $(selector).click()
        };

        // back button function
        self.back = function () {
            window.history.back();
        };

        // smooth scroll to element (copy from materialize scroll spy)
        self.scroll = function (id) {
            var c = $(id).offset().top + 1;
            $("html, body").animate({ scrollTop: c - 100 }, {
                duration: 400, queue: !1, easing: "easeOutCubic"
            });
        };

        // force reload state
        self.reloadState = function (stateName, params) {
            state.go(stateName, params, { reload: true });
        };

        self.print = function () { window.print() }

    }]);

// Callback when DOM $element ready
function reDraw($element) {
    var deferred = new $.Deferred();
    setTimeout(function () {
        var h = $element[0].offsetHeight;
        var s = $element[0].getComputedStyle;
        deferred.resolve();
    }, 0);
    return deferred.promise();
}

function setTimezone(timezone) {
    localStorage.timezone = timezone.split(/[UTC:]+/).join('').replace('−', '-').replace('±', '+');
};